# Aecidium, simple roguelike written in Rust

This project is a small roguelike written in the Rust language
using SDL2 for graphics. It is by no means comparable to more
established roguelikes and was written mainly as a learning
experience about Rust and roguelike development.

## Building and running

In order to build the project, you will need the Rust toolchain
and SDL2 libraries installed.

To build the project, first clone the Git repository:

    $ git clone https://gitlab.com/Samsai/rust-roguelike.git
    $ cd rust-roguelike

Then you can build the project using Cargo:

    $ cargo build --release

You can then launch the game:
    
    $ ./target/release/rust-roguelike


## Keybindings

### Movement and combat

To move around, use the arrow keys.

If you move into a square that is occupied by an NPC,
you will attack the NPC.

### Items and Inventory Management

To pick up an item from the ground, press the G key.

You can open your inventory using the I key and browse
your items with up and down arrows. Enter will equip/use
an item. If you press D you will drop the currently
selected item.
