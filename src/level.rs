extern crate rand;

use rand::prelude::*;

use helper::*;

pub const MAX_X: usize = 42;
pub const MAX_Y: usize = 30;
pub const MAX_X_i32: i32 = 42;
pub const MAX_Y_i32: i32 = 30;

#[derive(Copy, Clone, PartialEq)]
pub enum Tile {
    Void,
    Floor,
    Wall,
    Staircase,
}

pub struct Level {
    pub tiles: [[Tile; MAX_X]; MAX_Y],
    pub rooms: Vec<Room>,
}

impl Level {
    pub fn new() -> Level {
        return Level::new_specific(true);
    }

    pub fn new_specific(has_staircase: bool) -> Level {
        let tile_array: [[Tile; MAX_X]; MAX_Y] = [[Tile::Void; MAX_X]; MAX_Y];
        let rooms = Vec::new();

        let mut new_level = Level {
            tiles: tile_array,
            rooms: rooms,
        };

        // 3x3 grid to keep track of which cells have been populated
        let mut room_grid: [[bool; 3]; 3] = [[false; 3]; 3];

        let mut rng = thread_rng();

        // to connect the dungeon we need to keep track of the
        // previous room
        let mut previous_room = Room::new(0, 0, 0, 0); // create a bogus room because no null references
        let mut previous_set = false;

        for _i in 0..5 {
            let x = rng.gen_range(0, 3);
            let y = rng.gen_range(0, 3);

            if !room_grid[y][x] {
                room_grid[y][x] = true;

                let w = rng.gen_range(5, MAX_X / 3);
                let h = rng.gen_range(5, MAX_Y / 3);

                let room = Room::new(1 + x * (MAX_X / 3), 1 + y * (MAX_Y / 3), w - 1, h - 1);
                new_level.carve_room(&room);

                new_level.rooms.push(room.clone());

                if previous_set {
                    let (p_x, p_y) = previous_room.center();
                    let (c_x, c_y) = room.center();

                    new_level.carve_tunnel(c_x, c_y, p_x, p_y);

                    previous_room = room;
                } else {
                    previous_room = room;
                    previous_set = true;
                }
            }
        }

        let (rx, ry) = new_level.rooms.last().unwrap().center();

        for y in 0..MAX_Y {
            for x in 0..MAX_X {
                if new_level.get_tile(x as i32, y as i32) == Tile::Void
                    && new_level.is_adjacent_to_floor(x, y)
                {
                    new_level.tiles[y][x] = Tile::Wall;
                }
            }
        }

        if has_staircase {
            new_level.tiles[ry][rx] = Tile::Staircase;
        }

        return new_level;
    }

    pub fn get_tile(&self, x: i32, y: i32) -> Tile {
        self.tiles[y as usize][x as usize]
    }

    pub fn set_tile(&mut self, x: i32, y: i32, tile_type: Tile) {
        if y >= 0 && y < MAX_Y as i32 && x >= 0 && x < MAX_X as i32 {
            self.tiles[y as usize][x as usize] = tile_type;
        }
    }

    pub fn is_passable(&self, x: i32, y: i32) -> bool {
        if x < 0 || x >= MAX_X as i32 || y < 0 || y > MAX_Y as i32 {
            return false;
        }

        return match self.get_tile(x, y) {
            Tile::Void => false,
            Tile::Floor => true,
            Tile::Staircase => true,
            Tile::Wall => false,
        };
    }

    fn is_adjacent_to_floor(&self, x: usize, y: usize) -> bool {
        for (x, y) in surrounding_inc_diagonal((x as i32, y as i32)).iter() {
            if *x >= 0 && *x < MAX_X_i32 as i32 && *y >= 0 && *y < MAX_Y_i32 as i32 {
                if self.get_tile(*x, *y) == Tile::Floor {
                    return true;
                }
            }
        }

        return false;
    }

    fn carve_tunnel(&mut self, x1: usize, y1: usize, x2: usize, y2: usize) {
        self.tiles[y1][x1] = Tile::Floor;

        if x1 < x2 {
            self.carve_tunnel(x1 + 1, y1, x2, y2)
        } else if x1 > x2 {
            self.carve_tunnel(x1 - 1, y1, x2, y2)
        } else if y1 < y2 {
            self.carve_tunnel(x1, y1 + 1, x2, y2)
        } else if y1 > y2 {
            self.carve_tunnel(x1, y1 - 1, x2, y2)
        }
    }

    fn carve_room(&mut self, room: &Room) {
        for y in room.y..room.y + room.h {
            for x in room.x..room.x + room.w {
                self.tiles[y as usize][x as usize] = Tile::Floor;
            }
        }
    }

    // Bresenham's line drawing algorithm
    // - Draws a line from x0,y0 to x1,y1
    pub fn bresenham(&self, xy0: (i32, i32), xy1: (i32, i32), range: i32) -> Vec<(i32, i32)> {
        let mut points: Vec<(i32, i32)> = Vec::new();

        let (x0, y0) = xy0;
        let (x1, y1) = xy1;
        let mut r = range;

        let mut delta_x: i32 = x1 - x0;
        let ix = if delta_x > 0 { 1 } else { -1 };
        delta_x = delta_x.abs() << 1;

        let mut delta_y: i32 = y1 - y0;
        let iy = if delta_y > 0 { 1 } else { -1 };
        delta_y = delta_y.abs() << 1;

        points.push((x0, y0));

        if delta_x >= delta_y {
            let mut error = delta_y - (delta_x >> 1);
            let mut y = y0;
            let mut x = x0;

            while x != x1 {
                if error > 0 || (error == 0 && ix > 0) {
                    error -= delta_x;
                    y += iy;
                }

                error += delta_y;
                x += ix;
                r -= 1;

                if self.is_passable(x, y) && r >= 0 {
                    points.push((x, y));
                } else {
                    points.push((x, y));
                    break;
                }
            }
        } else {
            let mut error = delta_x - (delta_y >> 1);
            let mut x = x0;
            let mut y = y0;

            while y != y1 {
                if error > 0 || (error == 0 && iy > 0) {
                    error -= delta_y;
                    x += ix;
                }

                error += delta_x;
                y += iy;
                r -= 1;

                if self.is_passable(x, y) && r >= 0 {
                    points.push((x, y));
                } else {
                    points.push((x, y));
                    break;
                }
            }
        }

        return points;
    }

    // Raycasting algorithm for vision
    // Draw a line from origin to all sides of the game area
    // Collect all the points seen and return them
    pub fn raycast_vision(&self, x: i32, y: i32, range: i32) -> Vec<(i32, i32)> {
        let mut points: Vec<(i32, i32)> = Vec::new();

        for cx in 0..MAX_X_i32 {
            let new_points_top = self.bresenham((x, y), (cx, 0), range);
            let new_points_bottom = self.bresenham((x, y), (cx, MAX_Y_i32), range);

            for (nx, ny) in new_points_top {
                points.push((nx, ny));
            }

            for (nx, ny) in new_points_bottom {
                points.push((nx, ny));
            }
        }

        for cy in 0..MAX_Y_i32 {
            let new_points_left = self.bresenham((x, y), (0, cy), range);
            let new_points_right = self.bresenham((x, y), (MAX_X_i32, cy), range);

            for (nx, ny) in new_points_left {
                points.push((nx, ny));
            }

            for (nx, ny) in new_points_right {
                points.push((nx, ny));
            }
        }

        return points;
    }
}

#[derive(Clone)]
pub struct Room {
    x: usize,
    y: usize,
    w: usize,
    h: usize,
}

impl Room {
    pub fn new(x: usize, y: usize, w: usize, h: usize) -> Room {
        Room {
            x: x,
            y: y,
            w: w,
            h: h,
        }
    }

    pub fn center(&self) -> (usize, usize) {
        return (self.x + (self.w / 2), self.y + (self.h / 2));
    }

    pub fn any_point(&self) -> (usize, usize) {
        let mut rng = thread_rng();

        let x = self.x + rng.gen_range(0, self.w);
        let y = self.y + rng.gen_range(0, self.h);

        return (x, y);
    }
}
