extern crate anymap;

use anymap::AnyMap;

use std::any::TypeId;
use std::cell::Cell;
use std::cell::RefCell;
use std::collections::HashMap;
use std::collections::HashSet;
use std::sync::RwLock;

use level::*;
use *;

pub struct ECS {
    pub entities: HashSet<Entity>,
    recycle: RefCell<Vec<Entity>>,
    destroy_list: RefCell<Vec<Entity>>,
    create_list: RefCell<Vec<Entity>>,
    next_index: Cell<usize>,
    pub registered_components: Vec<TypeId>,
    pub components: AnyMap,
    pub delayed_components: AnyMap,
    pub delayed_removed_components: RwLock<HashMap<TypeId, Vec<Entity>>>,
    sync_closures: Vec<Box<dyn Fn(&AnyMap, &AnyMap, &RwLock<HashMap<TypeId, Vec<Entity>>>)>>,
}

impl ECS {
    pub fn new() -> ECS {
        return ECS {
            entities: HashSet::new(),
            recycle: RefCell::new(Vec::new()),
            destroy_list: RefCell::new(Vec::new()),
            create_list: RefCell::new(Vec::new()),
            next_index: Cell::new(0),
            registered_components: Vec::new(),
            components: AnyMap::new(),
            delayed_components: AnyMap::new(),
            delayed_removed_components: RwLock::new(HashMap::new()),
            sync_closures: Vec::new(),
        };
    }

    pub fn create_entity(&self) -> Entity {
        let mut recycle = self.recycle.borrow_mut();

        let entity = if recycle.is_empty() {
            let mut index = self.next_index.get();
            self.next_index.replace(index.overflowing_add(1).0);
            let generation = 0;

            Entity { index, generation }
        } else {
            let mut entity = recycle.pop().unwrap();
            entity.generation = entity.generation.overflowing_add(1).0;

            entity
        };

        let mut create_list = self.create_list.borrow_mut();
        create_list.push(entity.clone());

        return entity;
    }

    pub fn destroy_entity(&self, entity: &Entity) {
        let mut destroy_list = self.destroy_list.borrow_mut();
        destroy_list.push(entity.clone());
    }

    pub fn is_alive(&self, entity: &Entity) -> bool {
        return self.entities.contains(entity);
    }

    pub fn maintain(&mut self) {
        let mut destroy_list = self.destroy_list.borrow_mut();

        for entity in destroy_list.iter() {
            let mut recycle = self.recycle.borrow_mut();

            if self.entities.contains(entity) && !recycle.contains(&entity) {
                recycle.push(entity.clone());
            }

            self.entities.retain(|x| x != entity);
        }

        destroy_list.clear();

        let mut create_list = self.create_list.borrow_mut();

        for entity in create_list.iter() {
            self.entities.insert(entity.clone());
        }

        create_list.clear();

        for closure in self.sync_closures.iter() {
            closure(
                &self.components,
                &self.delayed_components,
                &self.delayed_removed_components,
            );
        }
    }

    pub fn register_component<C: 'static + Clone>(&mut self) {
        self.registered_components.push(TypeId::of::<C>());
        self.components.insert(RwLock::new(EntityMap::<C>::new()));
        self.delayed_components
            .insert(RwLock::new(EntityMap::<C>::new()));

        self.sync_closures.push(Box::new(
            |components, new_components, removed_components| {
                let mut current_comps = components
                    .get::<RwLock<EntityMap<C>>>()
                    .unwrap()
                    .write()
                    .unwrap();
                let mut new_comps = new_components
                    .get::<RwLock<EntityMap<C>>>()
                    .unwrap()
                    .write()
                    .unwrap();
                let mut removed_comps = removed_components.write().unwrap();

                for component in new_comps.storage.iter() {
                    if let Some((entity, comp)) = component {
                        current_comps.set(entity, comp.clone());
                    }
                }

                if let Some(entities) = removed_comps.get_mut(&TypeId::of::<C>()) {
                    for entity in entities.iter() {
                        current_comps.unset(&entity);
                    }

                    entities.clear();
                }

                new_comps.clear();
            },
        ));
    }

    pub fn get_components<C: 'static + Clone>(&self) -> Option<&RwLock<EntityMap<C>>> {
        return self.components.get::<RwLock<EntityMap<C>>>();
    }

    fn get_components_delay<C: 'static + Clone>(&self) -> Option<&RwLock<EntityMap<C>>> {
        return self.delayed_components.get::<RwLock<EntityMap<C>>>();
    }

    pub fn set_component<C: 'static + Clone>(&self, entity: &Entity, component: C) {
        let mut components = self.get_components::<C>().unwrap().write().unwrap();

        components.set(entity, component);
    }

    pub fn set_component_delayed<C: 'static + Clone>(&self, entity: &Entity, component: C) {
        let mut components = self.get_components_delay::<C>().unwrap().write().unwrap();

        components.set(entity, component);
    }

    pub fn remove_component_delayed<C: 'static + Clone>(&self, entity: &Entity) {
        let mut removal = self.delayed_removed_components.write().unwrap();

        if let Some(entities) = removal.get_mut(&TypeId::of::<C>()) {
            entities.push(entity.clone());

            return;
        }

        removal.insert(TypeId::of::<C>(), vec![entity.clone()]);
    }

    // TODO: Call this function automatically in a closure on maintain()
    pub fn sync_components<C: 'static + Clone>(&self) {
        let mut delayed_comps = self.get_components_delay::<C>().unwrap().write().unwrap();
        let mut removed_comps = self.delayed_removed_components.write().unwrap();
        let mut current_comps = self.get_components::<C>().unwrap().write().unwrap();

        for component in delayed_comps.storage.iter() {
            if let Some((entity, comp)) = component {
                current_comps.set(entity, comp.clone());
            }
        }

        if let Some(entities) = removed_comps.get(&TypeId::of::<C>()) {
            for entity in entities {
                current_comps.unset(&entity);
            }
        }

        removed_comps.clear();

        delayed_comps.clear();
    }

    pub fn has_component<C: 'static + Clone>(&self, entity: &Entity) -> bool {
        let components = self.get_components::<C>().unwrap().read().unwrap();

        return components.get(entity).is_some();
    }
}

#[derive(Clone, PartialEq, Eq, Hash)]
pub struct GenerationalIndex {
    index: usize,
    generation: usize,
}

pub type Entity = GenerationalIndex;

pub struct GenerationalVec<T> {
    pub storage: Vec<Option<(GenerationalIndex, T)>>,
}

impl<T: Clone> GenerationalVec<T> {
    pub fn new() -> GenerationalVec<T> {
        return GenerationalVec {
            storage: Vec::new(),
        };
    }

    pub fn clear(&mut self) {
        self.storage.clear();
    }

    pub fn set(&mut self, index: &GenerationalIndex, val: T) {
        if self.storage.len() <= index.index {
            self.storage.resize(index.index + 1, None);

            self.storage[index.index] = Some((index.clone(), val));
        } else {
            self.storage[index.index] = Some((index.clone(), val));
        }
    }

    pub fn unset(&mut self, index: &GenerationalIndex) {
        if self.storage.len() > index.index {
            self.storage[index.index] = None;
        }
    }

    pub fn get(&self, index: &GenerationalIndex) -> Option<&T> {
        if self.storage.len() <= index.index {
            return None;
        } else {
            if let Some((ref stored_index, ref value)) = self.storage[index.index] {
                if stored_index == index {
                    return Some(value);
                }
            }

            return None;
        }
    }

    pub fn get_mut(&mut self, index: &GenerationalIndex) -> Option<&mut T> {
        if self.storage.len() <= index.index {
            return None;
        } else {
            if let Some((ref mut stored_index, ref mut value)) = self.storage[index.index] {
                if stored_index == index {
                    return Some(value);
                }
            }

            return None;
        }
    }
}

pub type EntityMap<T> = GenerationalVec<T>;
