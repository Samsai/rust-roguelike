extern crate rand;

use rand::prelude::*;

use std::rc::Rc;

use helper::*;

use system::dungeon::get_npc_at;

use *;

#[derive(Clone)]
pub enum Item {
    Consumable(Rc<dyn Consumable>),
    Weapon(Rc<dyn Weapon>),
    Shield(Rc<dyn Shield>),
    Armor(Rc<dyn Armor>),
}

impl Item {
    pub fn name(&self) -> &str {
        return match self {
            Item::Consumable(consumable) => consumable.name(),
            Item::Weapon(weapon) => weapon.name(),
            Item::Shield(shield) => shield.name(),
            Item::Armor(armor) => armor.name(),
            _ => "none",
        };
    }

    pub fn description(&self) -> &str {
        return match self {
            Item::Consumable(consumable) => consumable.description(),
            Item::Weapon(weapon) => weapon.description(),
            Item::Shield(shield) => shield.description(),
            Item::Armor(armor) => armor.description(),
            _ => "none",
        };
    }
    pub fn sprite(&self) -> &str {
        return match self {
            Item::Consumable(consumable) => consumable.sprite(),
            Item::Weapon(weapon) => weapon.sprite(),
            Item::Shield(shield) => shield.sprite(),
            Item::Armor(armor) => armor.sprite(),
            _ => "none",
        };
    }
}

impl Component for Item {}

#[derive(Clone)]
pub enum Effect {
    Healing(i32),
    Poison(i32),
    Fire(i32),
}

pub trait Consumable {
    fn consume(&self, world: &World, entity: &Entity);
    fn name(&self) -> &str;
    fn description(&self) -> &str;
    fn sprite(&self) -> &str;
}

pub trait Weapon {
    fn attack(&self) -> Attack;
    fn name(&self) -> &str;
    fn description(&self) -> &str;
    fn sprite(&self) -> &str;
}

pub trait Shield {
    fn ac(&self) -> i32;
    fn name(&self) -> &str;
    fn description(&self) -> &str;
    fn sprite(&self) -> &str;
}

pub trait Armor {
    fn ac(&self) -> i32;
    fn name(&self) -> &str;
    fn description(&self) -> &str;
    fn sprite(&self) -> &str;
}

pub struct Dagger {}

impl Dagger {
    pub fn new() -> Dagger {
        return Dagger {};
    }
}

impl Weapon for Dagger {
    fn attack(&self) -> Attack {
        Attack::Physical("1d5")
    }

    fn name(&self) -> &str {
        return "Dagger";
    }

    fn description(&self) -> &str {
        return "A short, bladed weapon.";
    }

    fn sprite(&self) -> &str {
        return "dagger.png";
    }
}

pub fn create_dagger(world: &mut World) -> Entity {
    let entity = world.ecs.create_entity();

    let mut rng = thread_rng();

    let (rx, ry) = world.level.rooms[rng.gen_range(1, world.level.rooms.len())].any_point();

    world.ecs.set_component(
        &entity,
        Position {
            x: rx as i32,
            y: ry as i32,
        },
    );
    world
        .ecs
        .set_component(&entity, Item::Weapon(Rc::new(Dagger::new())));
    world.ecs.set_component(
        &entity,
        Renderable {
            sprite: String::from("dagger.png"),
            item: true,
        },
    );

    return entity;
}

pub struct Sword {}

impl Sword {
    pub fn new() -> Sword {
        return Sword {};
    }
}

impl Weapon for Sword {
    fn attack(&self) -> Attack {
        Attack::Physical("1d6 + 1")
    }

    fn name(&self) -> &str {
        return "Sword";
    }

    fn description(&self) -> &str {
        return "Your standard, knightly sword.";
    }

    fn sprite(&self) -> &str {
        return "shortsword.png";
    }
}

pub fn create_sword(world: &mut World) -> Entity {
    let entity = world.ecs.create_entity();

    let mut rng = thread_rng();

    let (rx, ry) = world.level.rooms[rng.gen_range(1, world.level.rooms.len())].any_point();

    world.ecs.set_component(
        &entity,
        Position {
            x: rx as i32,
            y: ry as i32,
        },
    );
    world
        .ecs
        .set_component(&entity, Item::Weapon(Rc::new(Sword::new())));
    world.ecs.set_component(
        &entity,
        Renderable {
            sprite: String::from("shortsword.png"),
            item: true,
        },
    );

    return entity;
}

pub struct Handaxe {}

impl Handaxe {
    pub fn new() -> Handaxe {
        return Handaxe {};
    }
}

impl Weapon for Handaxe {
    fn attack(&self) -> Attack {
        Attack::Physical("2d5")
    }

    fn name(&self) -> &str {
        return "Handaxe";
    }

    fn description(&self) -> &str {
        return "A powerful tool for hacking foes into pieces.";
    }

    fn sprite(&self) -> &str {
        return "handaxe.png";
    }
}

pub fn create_handaxe(world: &mut World) -> Entity {
    let entity = world.ecs.create_entity();

    let mut rng = thread_rng();

    let (rx, ry) = world.level.rooms[rng.gen_range(1, world.level.rooms.len())].any_point();

    world.ecs.set_component(
        &entity,
        Position {
            x: rx as i32,
            y: ry as i32,
        },
    );
    world
        .ecs
        .set_component(&entity, Item::Weapon(Rc::new(Handaxe::new())));
    world.ecs.set_component(
        &entity,
        Renderable {
            sprite: String::from("handaxe.png"),
            item: true,
        },
    );

    return entity;
}

pub struct HealingPotion {}

impl HealingPotion {
    pub fn new() -> HealingPotion {
        return HealingPotion {};
    }
}

impl Consumable for HealingPotion {
    fn consume(&self, world: &World, entity: &Entity) {
        let mut health_components = world
            .ecs
            .get_components::<Health>()
            .unwrap()
            .write()
            .unwrap();
        let mut health = health_components.get_mut(entity).unwrap();

        health.hp += 10;

        if health.hp > health.max_hp {
            health.hp = health.max_hp;
        }
    }

    fn name(&self) -> &str {
        return "Healing Potion";
    }

    fn description(&self) -> &str {
        return "A potion that will heal wounds.";
    }

    fn sprite(&self) -> &str {
        return "green_potion.png";
    }
}

pub fn create_healing_potion(world: &mut World) -> Entity {
    let entity = world.ecs.create_entity();

    let mut rng = thread_rng();

    let (rx, ry) = world.level.rooms[rng.gen_range(1, world.level.rooms.len())].any_point();

    world.ecs.set_component(
        &entity,
        Position {
            x: rx as i32,
            y: ry as i32,
        },
    );
    world
        .ecs
        .set_component(&entity, Item::Consumable(Rc::new(HealingPotion::new())));
    world.ecs.set_component(
        &entity,
        Renderable {
            sprite: String::from("green_potion.png"),
            item: true,
        },
    );

    return entity;
}

pub struct LifePotion {}

impl LifePotion {
    pub fn new() -> LifePotion {
        return LifePotion {};
    }
}

impl Consumable for LifePotion {
    fn consume(&self, world: &World, entity: &Entity) {
        let mut health_components = world
            .ecs
            .get_components::<Health>()
            .unwrap()
            .write()
            .unwrap();
        let mut health = health_components.get_mut(entity).unwrap();

        health.max_hp += 5;
        health.hp += 5;
    }

    fn name(&self) -> &str {
        return "Life Potion";
    }

    fn description(&self) -> &str {
        return "A potion that will extend the life itself.";
    }

    fn sprite(&self) -> &str {
        return "red_potion.png";
    }
}

pub fn create_life_potion(world: &mut World) -> Entity {
    let entity = world.ecs.create_entity();

    let mut rng = thread_rng();

    let (rx, ry) = world.level.rooms[rng.gen_range(1, world.level.rooms.len())].any_point();

    world.ecs.set_component(
        &entity,
        Position {
            x: rx as i32,
            y: ry as i32,
        },
    );
    world
        .ecs
        .set_component(&entity, Item::Consumable(Rc::new(LifePotion::new())));
    world.ecs.set_component(
        &entity,
        Renderable {
            sprite: String::from("red_potion.png"),
            item: true,
        },
    );

    return entity;
}

pub struct CurePotion {}

impl CurePotion {
    pub fn new() -> CurePotion {
        return CurePotion {};
    }
}

impl Consumable for CurePotion {
    fn consume(&self, world: &World, entity: &Entity) {
        let mut effects_components = world
            .ecs
            .get_components::<Effects>()
            .unwrap()
            .write()
            .unwrap();
        if let Some(effects) = effects_components.get_mut(entity) {
            effects.effects.retain(|x| {
                if let Effect::Poison(_value) = x {
                    false
                } else {
                    true
                }
            });
        }
    }

    fn name(&self) -> &str {
        return "Cure Potion";
    }

    fn description(&self) -> &str {
        return "A potion that will cure you of any poisoning.";
    }

    fn sprite(&self) -> &str {
        return "cyan_potion.png";
    }
}

pub fn create_cure_potion(world: &mut World) -> Entity {
    let entity = world.ecs.create_entity();

    let mut rng = thread_rng();

    let (rx, ry) = world.level.rooms[rng.gen_range(1, world.level.rooms.len())].any_point();

    world.ecs.set_component(
        &entity,
        Position {
            x: rx as i32,
            y: ry as i32,
        },
    );
    world
        .ecs
        .set_component(&entity, Item::Consumable(Rc::new(CurePotion::new())));
    world.ecs.set_component(
        &entity,
        Renderable {
            sprite: String::from("cyan_potion.png"),
            item: true,
        },
    );

    return entity;
}

pub struct FireResistancePotion {}

impl FireResistancePotion {
    pub fn new() -> FireResistancePotion {
        return FireResistancePotion {};
    }
}

impl Consumable for FireResistancePotion {
    fn consume(&self, world: &World, entity: &Entity) {
        world.ecs.set_component_delayed(
            entity,
            FireResistant {
                turns: 10,
                permanent: false,
            },
        );
    }

    fn name(&self) -> &str {
        return "Fire Resistance Potion";
    }

    fn description(&self) -> &str {
        return "A potion that will makes you immune to heat and fire.";
    }

    fn sprite(&self) -> &str {
        return "blue_potion.png";
    }
}

pub fn create_fire_resistance_potion(world: &mut World) -> Entity {
    let entity = world.ecs.create_entity();

    let mut rng = thread_rng();

    let (rx, ry) = world.level.rooms[rng.gen_range(1, world.level.rooms.len())].any_point();

    world.ecs.set_component(
        &entity,
        Position {
            x: rx as i32,
            y: ry as i32,
        },
    );
    world.ecs.set_component(
        &entity,
        Item::Consumable(Rc::new(FireResistancePotion::new())),
    );
    world.ecs.set_component(
        &entity,
        Renderable {
            sprite: String::from("blue_potion.png"),
            item: true,
        },
    );

    return entity;
}

pub struct Torch {}

impl Torch {
    pub fn new() -> Torch {
        return Torch {};
    }
}

impl Consumable for Torch {
    fn consume(&self, world: &World, entity: &Entity) {
        let mut vision_components = world
            .ecs
            .get_components::<Vision>()
            .unwrap()
            .write()
            .unwrap();

        if let Some(vision) = vision_components.get_mut(entity) {
            vision.range = vision.max_range;
        }
    }

    fn name(&self) -> &str {
        return "Torch";
    }

    fn description(&self) -> &str {
        return "Dungeons are dark. This will help you see.";
    }

    fn sprite(&self) -> &str {
        return "torch.png";
    }
}

pub fn create_torch(world: &mut World) -> Entity {
    let entity = world.ecs.create_entity();

    let mut rng = thread_rng();

    let (rx, ry) = world.level.rooms[rng.gen_range(1, world.level.rooms.len())].any_point();

    world.ecs.set_component(
        &entity,
        Position {
            x: rx as i32,
            y: ry as i32,
        },
    );
    world
        .ecs
        .set_component(&entity, Item::Consumable(Rc::new(Torch::new())));
    world.ecs.set_component(
        &entity,
        Renderable {
            sprite: String::from("torch.png"),
            item: true,
        },
    );

    return entity;
}

pub struct MappingScroll {}

impl MappingScroll {
    pub fn new() -> MappingScroll {
        return MappingScroll {};
    }
}

impl Consumable for MappingScroll {
    fn consume(&self, world: &World, entity: &Entity) {
        let mut tiles_memory = world.tiles_memory.borrow_mut();

        tiles_memory.clear();

        for y in 0..level::MAX_Y {
            for x in 0..level::MAX_X {
                tiles_memory.push((x as i32, y as i32));
            }
        }
    }

    fn name(&self) -> &str {
        return "Scroll of Mapping";
    }

    fn description(&self) -> &str {
        return "Gives the reader insight into the dungeon's layout.";
    }

    fn sprite(&self) -> &str {
        return "scroll.png";
    }
}

pub fn create_mapping_scroll(world: &mut World) -> Entity {
    let entity = world.ecs.create_entity();

    let mut rng = thread_rng();

    let (rx, ry) = world.level.rooms[rng.gen_range(1, world.level.rooms.len())].any_point();

    world.ecs.set_component(
        &entity,
        Position {
            x: rx as i32,
            y: ry as i32,
        },
    );
    world
        .ecs
        .set_component(&entity, Item::Consumable(Rc::new(MappingScroll::new())));
    world.ecs.set_component(
        &entity,
        Renderable {
            sprite: String::from("scroll.png"),
            item: true,
        },
    );

    return entity;
}

pub struct TeleportScroll {}

impl TeleportScroll {
    pub fn new() -> TeleportScroll {
        return TeleportScroll {};
    }
}

impl Consumable for TeleportScroll {
    fn consume(&self, world: &World, entity: &Entity) {
        let mut rng = thread_rng();

        loop {
            let (x, y) = world.level.rooms[rng.gen_range(0, world.level.rooms.len())].any_point();

            if let None = get_npc_at(
                world,
                &Position {
                    x: x as i32,
                    y: y as i32,
                },
            ) {
                let mut position_components = world
                    .ecs
                    .get_components::<Position>()
                    .unwrap()
                    .write()
                    .unwrap();

                let mut pos = position_components.get_mut(entity).unwrap();

                pos.x = x as i32;
                pos.y = y as i32;

                break;
            }
        }
    }

    fn name(&self) -> &str {
        return "Scroll of Teleportation";
    }

    fn description(&self) -> &str {
        return "Moves the reader into a random room of the dungeon.";
    }

    fn sprite(&self) -> &str {
        return "scroll.png";
    }
}

pub fn create_teleport_scroll(world: &mut World) -> Entity {
    let entity = world.ecs.create_entity();

    let mut rng = thread_rng();

    let (rx, ry) = world.level.rooms[rng.gen_range(1, world.level.rooms.len())].any_point();

    world.ecs.set_component(
        &entity,
        Position {
            x: rx as i32,
            y: ry as i32,
        },
    );
    world
        .ecs
        .set_component(&entity, Item::Consumable(Rc::new(TeleportScroll::new())));
    world.ecs.set_component(
        &entity,
        Renderable {
            sprite: String::from("scroll.png"),
            item: true,
        },
    );

    return entity;
}

pub struct WebScroll {}

impl WebScroll {
    pub fn new() -> WebScroll {
        return WebScroll {};
    }
}

impl Consumable for WebScroll {
    fn consume(&self, world: &World, entity: &Entity) {
        let position_components = world
            .ecs
            .get_components::<Position>()
            .unwrap()
            .read()
            .unwrap();

        let pos = position_components.get(entity).unwrap();

        let web = world.ecs.create_entity();

        world.ecs.set_component_delayed(&web, pos.clone());
        world.ecs.set_component_delayed(
            &web,
            Renderable {
                sprite: String::from("web.png"),
                item: false,
            },
        );
        world.ecs.set_component_delayed(&web, Web {});

        world.ecs.set_component_delayed(
            entity,
            EntangleResistant {
                turns: 1,
                permanent: false,
            },
        );
    }

    fn name(&self) -> &str {
        return "Scroll of Spiderwebs";
    }

    fn description(&self) -> &str {
        return "Creates a spiderweb in the reader's position.";
    }

    fn sprite(&self) -> &str {
        return "scroll.png";
    }
}

pub fn create_web_scroll(world: &mut World) -> Entity {
    let entity = world.ecs.create_entity();

    let mut rng = thread_rng();

    let (rx, ry) = world.level.rooms[rng.gen_range(1, world.level.rooms.len())].any_point();

    world.ecs.set_component(
        &entity,
        Position {
            x: rx as i32,
            y: ry as i32,
        },
    );
    world
        .ecs
        .set_component(&entity, Item::Consumable(Rc::new(WebScroll::new())));
    world.ecs.set_component(
        &entity,
        Renderable {
            sprite: String::from("scroll.png"),
            item: true,
        },
    );

    return entity;
}
pub struct MagicalCrystal {}

impl MagicalCrystal {
    pub fn new() -> MagicalCrystal {
        return MagicalCrystal {};
    }
}

impl Consumable for MagicalCrystal {
    fn consume(&self, world: &World, entity: &Entity) {
        // TODO: End the game victoriously

        world.victory.replace(true);
    }

    fn name(&self) -> &str {
        return "Magical Crystal";
    }

    fn description(&self) -> &str {
        return "The magical crystal that will allow you to leave this realm.";
    }

    fn sprite(&self) -> &str {
        return "treasure.png";
    }
}

pub fn create_treasure(world: &mut World) -> Entity {
    let entity = world.ecs.create_entity();

    let mut rng = thread_rng();

    let (rx, ry) = world.level.rooms[rng.gen_range(1, world.level.rooms.len())].any_point();

    world.ecs.set_component(
        &entity,
        Position {
            x: rx as i32,
            y: ry as i32,
        },
    );
    world
        .ecs
        .set_component(&entity, Item::Consumable(Rc::new(MagicalCrystal::new())));
    world.ecs.set_component(
        &entity,
        Renderable {
            sprite: String::from("treasure.png"),
            item: true,
        },
    );

    return entity;
}

pub struct Roundshield {}

impl Roundshield {
    pub fn new() -> Roundshield {
        return Roundshield {};
    }
}

impl Shield for Roundshield {
    fn ac(&self) -> i32 {
        2
    }

    fn name(&self) -> &str {
        "Roundshield"
    }

    fn description(&self) -> &str {
        return "A small, wooden shield that gives modest protection.";
    }

    fn sprite(&self) -> &str {
        "roundshield.png"
    }
}

pub fn create_roundshield(world: &mut World) -> Entity {
    let entity = world.ecs.create_entity();

    let mut rng = thread_rng();

    let (rx, ry) = world.level.rooms[rng.gen_range(1, world.level.rooms.len())].any_point();

    world.ecs.set_component(
        &entity,
        Position {
            x: rx as i32,
            y: ry as i32,
        },
    );
    world
        .ecs
        .set_component(&entity, Item::Shield(Rc::new(Roundshield::new())));
    world.ecs.set_component(
        &entity,
        Renderable {
            sprite: String::from("roundshield.png"),
            item: true,
        },
    );

    return entity;
}

pub struct Kiteshield {}

impl Kiteshield {
    pub fn new() -> Kiteshield {
        return Kiteshield {};
    }
}

impl Shield for Kiteshield {
    fn ac(&self) -> i32 {
        5
    }

    fn name(&self) -> &str {
        "Kiteshield"
    }

    fn description(&self) -> &str {
        return "A larger shield that gives good protection.";
    }

    fn sprite(&self) -> &str {
        "kiteshield.png"
    }
}

pub fn create_kiteshield(world: &mut World) -> Entity {
    let entity = world.ecs.create_entity();

    let mut rng = thread_rng();

    let (rx, ry) = world.level.rooms[rng.gen_range(1, world.level.rooms.len())].any_point();

    world.ecs.set_component(
        &entity,
        Position {
            x: rx as i32,
            y: ry as i32,
        },
    );
    world
        .ecs
        .set_component(&entity, Item::Shield(Rc::new(Kiteshield::new())));
    world.ecs.set_component(
        &entity,
        Renderable {
            sprite: String::from("kiteshield.png"),
            item: true,
        },
    );

    return entity;
}

pub struct LeatherArmor {}

impl LeatherArmor {
    pub fn new() -> LeatherArmor {
        return LeatherArmor {};
    }
}

impl Armor for LeatherArmor {
    fn ac(&self) -> i32 {
        2
    }

    fn name(&self) -> &str {
        "Leather Armor"
    }

    fn description(&self) -> &str {
        return "A chestpiece made of leather. Better than nothing.";
    }

    fn sprite(&self) -> &str {
        "leather-armour.png"
    }
}

pub fn create_leather_armor(world: &mut World) -> Entity {
    let entity = world.ecs.create_entity();

    let mut rng = thread_rng();

    let (rx, ry) = world.level.rooms[rng.gen_range(1, world.level.rooms.len())].any_point();

    world.ecs.set_component(
        &entity,
        Position {
            x: rx as i32,
            y: ry as i32,
        },
    );
    world
        .ecs
        .set_component(&entity, Item::Armor(Rc::new(LeatherArmor::new())));
    world.ecs.set_component(
        &entity,
        Renderable {
            sprite: String::from("leather-armour.png"),
            item: true,
        },
    );

    return entity;
}

pub struct ChainMail {}

impl ChainMail {
    pub fn new() -> ChainMail {
        return ChainMail {};
    }
}

impl Armor for ChainMail {
    fn ac(&self) -> i32 {
        5
    }

    fn name(&self) -> &str {
        "Chain Mail"
    }

    fn description(&self) -> &str {
        return "A chestpiece made of chain links. Decent protection.";
    }

    fn sprite(&self) -> &str {
        "chain-mail.png"
    }
}

pub fn create_chain_mail(world: &mut World) -> Entity {
    let entity = world.ecs.create_entity();

    let mut rng = thread_rng();

    let (rx, ry) = world.level.rooms[rng.gen_range(1, world.level.rooms.len())].any_point();

    world.ecs.set_component(
        &entity,
        Position {
            x: rx as i32,
            y: ry as i32,
        },
    );
    world
        .ecs
        .set_component(&entity, Item::Armor(Rc::new(ChainMail::new())));
    world.ecs.set_component(
        &entity,
        Renderable {
            sprite: String::from("chain-mail.png"),
            item: true,
        },
    );

    return entity;
}
