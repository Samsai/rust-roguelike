extern crate sdl2;

use sdl2::keyboard::Keycode;

use component::*;

#[derive(Clone, Copy, PartialEq)]
pub enum Direction {
    Up,
    Down,
    Left,
    Right,
    Stop,
}

pub fn move_coord(coord: (i32, i32), dir: Direction) -> (i32, i32) {
    let (x, y) = coord;
    match dir {
        Direction::Up => (x, y - 1),
        Direction::Down => (x, y + 1),
        Direction::Left => (x - 1, y),
        Direction::Right => (x + 1, y),
        _ => (x, y),
    }
}

pub fn direction_priorities(begin: (i32, i32), target: (i32, i32)) -> [Direction; 4] {
    let (bx, by) = begin;
    let (tx, ty) = target;

    let mut directions: [Direction; 4] = [Direction::Up; 4];

    let dx = tx - bx;
    let dy = ty - by;

    if dx.abs() > dy.abs() {
        if dx < 0 {
            directions[0] = Direction::Left;
            directions[2] = Direction::Right;
        } else {
            directions[0] = Direction::Right;
            directions[2] = Direction::Left;
        }

        if dy < 0 {
            directions[1] = Direction::Up;
            directions[3] = Direction::Down;
        } else {
            directions[1] = Direction::Down;
            directions[3] = Direction::Up;
        }
    } else {
        if dy < 0 {
            directions[0] = Direction::Up;
            directions[2] = Direction::Down;
        } else {
            directions[0] = Direction::Down;
            directions[2] = Direction::Up;
        }

        if dx < 0 {
            directions[1] = Direction::Left;
            directions[3] = Direction::Right;
        } else {
            directions[1] = Direction::Right;
            directions[3] = Direction::Left;
        }
    }

    return directions;
}

pub fn keycode_to_direction(key: Keycode) -> Direction {
    match key {
        Keycode::Up => Direction::Up,
        Keycode::Down => Direction::Down,
        Keycode::Left => Direction::Left,
        Keycode::Right => Direction::Right,
        Keycode::H => Direction::Left,
        Keycode::L => Direction::Right,
        Keycode::J => Direction::Down,
        Keycode::K => Direction::Up,
        _ => Direction::Stop,
    }
}

pub fn is_keycode_direction(key: Keycode) -> bool {
    match key {
        Keycode::Up => true,
        Keycode::Down => true,
        Keycode::Left => true,
        Keycode::Right => true,
        Keycode::H => true,
        Keycode::L => true,
        Keycode::J => true,
        Keycode::K => true,
        _ => false,
    }
}

fn invert(direction: Direction) -> Direction {
    match direction {
        Direction::Up => Direction::Down,
        Direction::Down => Direction::Up,
        Direction::Left => Direction::Right,
        Direction::Right => Direction::Left,
        Direction::Stop => Direction::Stop,
    }
}

fn surrounding(coord: (i32, i32)) -> [(Direction, (i32, i32)); 4] {
    let (x, y) = coord;

    return [
        (Direction::Left, (x - 1, y)),
        (Direction::Right, (x + 1, y)),
        (Direction::Up, (x, y - 1)),
        (Direction::Down, (x, y + 1)),
    ];
}

pub fn surrounding_no_dirs(coord: (i32, i32)) -> [(i32, i32); 4] {
    let (x, y) = coord;

    return [(x - 1, y), (x + 1, y), (x, y - 1), (x, y + 1)];
}

pub fn surrounding_inc_diagonal(coord: (i32, i32)) -> [(i32, i32); 8] {
    let (x, y) = coord;

    return [
        (x - 1, y),
        (x + 1, y),
        (x, y - 1),
        (x, y + 1),
        (x - 1, y - 1),
        (x + 1, y + 1),
        (x - 1, y + 1),
        (x + 1, y - 1),
    ];
}

pub fn are_adjacent(pos1: &Position, pos2: &Position) -> bool {
    let (x1, y1) = (pos1.x, pos1.y);
    let (x2, y2) = (pos2.x, pos2.y);

    surrounding_no_dirs((x1, y1))
        .iter()
        .find(|x| x.0 == x2 && x.1 == y2)
        .is_some()
}
