use std::cell::RefCell;
use std::slice::Iter;
use std::vec::Vec;

pub struct Log {
    pub messages: RefCell<Vec<String>>,
}

impl Log {
    pub fn new() -> Log {
        return Log {
            messages: RefCell::new(Vec::new()),
        };
    }

    pub fn write(&self, message: String) {
        self.messages.borrow_mut().push(message);
    }

    pub fn read(&self) -> Vec<String> {
        return self.messages.borrow().clone();
    }
}
