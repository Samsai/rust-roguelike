extern crate sdl2;

extern crate lib_dice;

extern crate rand;

use sdl2::keyboard::Keycode;
use sdl2::pixels::Color;
use sdl2::rect::Rect;

use rand::*;

use std::collections::BinaryHeap;
use std::collections::HashMap;
use std::collections::HashSet;
use std::collections::VecDeque;

use std::rc::Rc;

use super::System;

use World;

use entity::*;

use component::*;
use creatures::*;
use helper::*;
use item::*;
use level::*;

pub fn init_new_game(world: &mut World) {
    {
        let mut log = world.log.borrow_mut();

        log.clear();
    }

    world.depth = 1;

    world.ecs.destroy_entity(&world.player);

    world.player = world.ecs.create_entity();
    create_player(world);
    generate_level(world);
}

pub fn spawn_creatures(world: &mut World) {
    let mut points = world.depth * 5;

    while points > 0 {
        let val = if points > 1 {
            if points > 50 {
                world.rng.gen_range(1, 50)
            } else {
                world.rng.gen_range(1, points)
            }
        } else {
            1
        };

        match val {
            1..=9 => {
                create_bat(world);
            }
            10..=13 => {
                create_slime(world);
            }
            14..=20 => {
                create_thief(world);
            }
            21..=29 => {
                create_spider(world);
            }
            30..=35 => {
                create_skeleton(world);
            }
            36..=40 => {
                create_ghost(world);
            }
            41..=44 => {
                create_beholder(world);
            }
            45..=50 => {
                create_dragon(world);
            }
            _ => {}
        }

        points -= val;
    }
}

pub fn spawn_items(world: &mut World) {
    let mut points = world.depth * 10;

    while points > 0 {
        let val = if points > 1 {
            if points > 50 {
                world.rng.gen_range(1, 50)
            } else {
                world.rng.gen_range(1, points)
            }
        } else {
            1
        };

        match val {
            5..=7 => {
                create_torch(world);
            }
            8..=10 => {
                create_dagger(world);
            }
            11..=11 => {
                create_healing_potion(world);
            }
            12..=18 => {
                create_sword(world);
            }
            19..=20 => {
                create_roundshield(world);
            }
            21..=21 => {
                create_cure_potion(world);
            }
            22..=24 => {
                create_leather_armor(world);
            }
            25..=27 => {
                create_life_potion(world);
            }
            28..=29 => {
                create_mapping_scroll(world);
            }
            30..=32 => {
                create_web_scroll(world);
            }
            33..=35 => {
                create_teleport_scroll(world);
            }
            36..=39 => {
                create_kiteshield(world);
            }
            40..=40 => {
                create_fire_resistance_potion(world);
            }
            41..=44 => {
                create_chain_mail(world);
            }
            45..=50 => {
                create_handaxe(world);
            }
            _ => {}
        }

        points -= val;
    }

    if world.depth == 25 {
        create_treasure(world);
    }
}

pub fn generate_level(world: &mut World) {
    if world.depth < 25 {
        world.level = Level::new();
    } else {
        world.level = Level::new_specific(false);
    }

    {
        let mut tiles_memory = world.tiles_memory.borrow_mut();
        tiles_memory.clear();
    }

    // Clean up old NPCs and items and webs
    {
        let npc_components = world.ecs.get_components::<NPC>().unwrap().read().unwrap();
        let item_components = world.ecs.get_components::<Item>().unwrap().read().unwrap();
        let web_components = world.ecs.get_components::<Web>().unwrap().read().unwrap();

        for component in npc_components.storage.iter() {
            if let Some((entity, _)) = component {
                world.ecs.destroy_entity(entity);
            }
        }

        for component in item_components.storage.iter() {
            if let Some((entity, _)) = component {
                world.ecs.destroy_entity(entity);
            }
        }

        for component in web_components.storage.iter() {
            if let Some((entity, _)) = component {
                world.ecs.destroy_entity(entity);
            }
        }
    }

    // Reset player position
    {
        let mut position_components = world
            .ecs
            .get_components::<Position>()
            .unwrap()
            .write()
            .unwrap();
        let mut player_pos = position_components.get_mut(&world.player).unwrap();

        let (x, y) = world.level.rooms[0].center();

        player_pos.x = x as i32;
        player_pos.y = y as i32;
    }

    {
        spawn_creatures(world);
        spawn_items(world);
    }

    world.ecs.maintain();
}

pub struct RenderDungeonSystem {}

impl RenderDungeonSystem {
    pub fn new() -> RenderDungeonSystem {
        return RenderDungeonSystem {};
    }
}

impl System for RenderDungeonSystem {
    fn run(&mut self, world: &mut World) {
        let position_components = world
            .ecs
            .get_components::<Position>()
            .unwrap()
            .read()
            .unwrap();
        let vision_components = world
            .ecs
            .get_components::<Vision>()
            .unwrap()
            .read()
            .unwrap();

        let player_pos = position_components.get(&world.player).unwrap();

        let player_vision = vision_components.get(&world.player).unwrap();

        let points = world
            .level
            .raycast_vision(player_pos.x, player_pos.y, player_vision.range);
        let mut tiles_memory = world.tiles_memory.borrow_mut();

        for point in &points {
            if !tiles_memory.contains(&point) {
                tiles_memory.push(point.clone());
            }
        }

        for y in 0..(MAX_Y as i32) {
            for x in 0..(MAX_X as i32) {
                if tiles_memory.contains(&(x, y)) {
                    let tile = world.level.tiles[y as usize][x as usize];
                    match tile {
                        Tile::Void => {
                            world.graphics.canvas.set_draw_color(Color::RGB(0, 0, 0));
                        }
                        Tile::Floor => {
                            world
                                .graphics
                                .canvas
                                .set_draw_color(Color::RGB(100, 100, 100));
                        }
                        _ => {
                            world.graphics.canvas.set_draw_color(Color::RGB(255, 0, 0));
                        }
                    }

                    world.graphics.canvas.fill_rect(Rect::new(
                        (x as i32) * 20,
                        (y as i32) * 20,
                        20,
                        20,
                    ));
                    if tile == Tile::Staircase {
                        world.graphics.draw_sprite("stairs_dark.png", x, y);
                    } else if tile == Tile::Wall {
                        world.graphics.draw_sprite("wall_dark.png", x, y);
                    }
                }
            }
        }

        for y in 0..(MAX_Y as i32) {
            for x in 0..(MAX_X as i32) {
                if points.contains(&(x, y)) {
                    let tile = world.level.tiles[y as usize][x as usize];
                    match tile {
                        Tile::Void => {
                            world.graphics.canvas.set_draw_color(Color::RGB(0, 0, 0));
                        }
                        Tile::Floor => {
                            world
                                .graphics
                                .canvas
                                .set_draw_color(Color::RGB(255, 255, 255));
                        }
                        _ => {
                            world.graphics.canvas.set_draw_color(Color::RGB(255, 0, 0));
                        }
                    }

                    world.graphics.canvas.fill_rect(Rect::new(
                        (x as i32) * 20,
                        (y as i32) * 20,
                        20,
                        20,
                    ));

                    if tile == Tile::Staircase {
                        world.graphics.draw_sprite("stairs.png", x, y);
                    } else if tile == Tile::Wall {
                        world.graphics.draw_sprite("wall.png", x, y);
                    }
                }
            }
        }

        let ecs = &world.ecs;
        let graphics = &mut world.graphics;

        let renderable_components = world
            .ecs
            .get_components::<Renderable>()
            .unwrap()
            .read()
            .unwrap();
        renderable_components.storage.iter().for_each(|component| {
            if let Some((entity, renderable)) = component {
                if ecs.is_alive(entity) {
                    if let Some(position) = position_components.get(entity) {
                        if (points.contains(&(position.x, position.y)))
                            || tiles_memory.contains(&(position.x, position.y)) && renderable.item
                        {
                            graphics.draw_sprite(
                                renderable.sprite.as_str(),
                                position.x,
                                position.y,
                            );
                        }
                    }
                }
            }
        });
    }

    fn post_run(&mut self, world: &mut World) {}
}

pub struct RenderHUDSystem {}

impl RenderHUDSystem {
    pub fn new() -> RenderHUDSystem {
        return RenderHUDSystem {};
    }
}

impl System for RenderHUDSystem {
    fn run(&mut self, world: &mut World) {
        let mut y = 740;

        // player information side-panel background
        world
            .graphics
            .canvas
            .set_draw_color(Color::RGB(0, 100, 100));
        world.graphics.canvas.fill_rect(Rect::new(840, 0, 184, 768));

        // message log background colour
        world
            .graphics
            .canvas
            .set_draw_color(Color::RGB(0, 100, 100));
        world
            .graphics
            .canvas
            .fill_rect(Rect::new(0, 600, 1024, (768 - 600)));

        let log = world.log.borrow();

        // Battle log
        for msg in log.iter().rev().take(8) {
            let text_info = world.graphics.draw_text(&msg, (20, y)).unwrap();

            y -= text_info.height as i32;
        }

        let player_ac = calculate_ac(&world.ecs, &world.player);

        let health_components = world
            .ecs
            .get_components::<Health>()
            .unwrap()
            .read()
            .unwrap();
        let armed_components = world.ecs.get_components::<Armed>().unwrap().read().unwrap();
        let shielded_components = world
            .ecs
            .get_components::<Shielded>()
            .unwrap()
            .read()
            .unwrap();

        let effects_components = world
            .ecs
            .get_components::<Effects>()
            .unwrap()
            .read()
            .unwrap();
        let player_health = health_components.get(&world.player).unwrap();

        // Player info
        let hp_text = &format!("HP: {}/{}", player_health.hp, player_health.max_hp);
        world.graphics.draw_text(&hp_text, (850, 20));

        let hp_text = &format!("AC: {}", player_ac);
        world.graphics.draw_text(&hp_text, (850, 40));

        world
            .graphics
            .draw_text(&format!("Depth: {}", world.depth), (850, 60));

        let equipped_weapon = armed_components.get(&world.player);

        let weapon_text = match equipped_weapon {
            Some(ref armed) => format!("Wp: {}", armed.weapon.name()),
            None => format!("Wp: None"),
        };

        world.graphics.draw_text(&weapon_text, (850, 80));

        let equipped_shield = shielded_components.get(&world.player);

        let shield_text = match equipped_shield {
            Some(ref shielded) => format!("Sh: {}", shielded.shield.name()),
            None => format!("Sh: None"),
        };

        world.graphics.draw_text(&shield_text, (850, 100));

        let mut y = 120;

        if let Some(effects) = effects_components.get(&world.player) {
            for effect in effects.effects.iter() {
                match effect {
                    Effect::Healing(value) => {
                        world
                            .graphics
                            .draw_text(&format!("Heal: {}", value), (850, y));
                    }
                    Effect::Poison(value) => {
                        world.graphics.draw_text_rgb(
                            &format!("Poison: {}", value),
                            (850, y),
                            Color::RGB(0, 255, 0),
                        );
                    }
                    Effect::Fire(value) => {
                        world.graphics.draw_text_rgb(
                            &format!("Fire: {}", value),
                            (850, y),
                            Color::RGB(255, 0, 0),
                        );
                    }
                }

                y += 20;
            }
        }
    }

    fn post_run(&mut self, world: &mut World) {}
}

pub struct PlayerInputSystem {}

impl PlayerInputSystem {
    pub fn new() -> PlayerInputSystem {
        return PlayerInputSystem {};
    }
}

impl System for PlayerInputSystem {
    fn run(&mut self, world: &mut World) {
        let ecs = &world.ecs;

        let mut log = world.log.borrow_mut();

        let mut inventory_components = ecs.get_components::<Inventory>().unwrap().write().unwrap();

        let mut inventory = inventory_components.get_mut(&world.player).unwrap();

        let mut item_components = ecs.get_components::<Item>().unwrap().read().unwrap();
        let mut attacking_components = ecs.get_components::<Attacking>().unwrap().read().unwrap();

        let mut vision_components = ecs.get_components::<Vision>().unwrap().write().unwrap();

        world.turns += 1;

        if world.turns % 50 == 0 {
            let mut player_vision = vision_components.get_mut(&world.player).unwrap();

            if player_vision.range > 1 {
                player_vision.range -= 1;
            }
        }

        if let Some(key) = world.key {
            if is_keycode_direction(key) {
                let (mut x, mut y) = {
                    let mut position_components =
                        ecs.get_components::<Position>().unwrap().write().unwrap();
                    let entangled_components =
                        ecs.get_components::<Entangled>().unwrap().read().unwrap();
                    if let Some(entangled) = entangled_components.get(&world.player) {
                        log.push(String::from("You are stuck!"));

                        return;
                    }

                    let position = position_components.get(&world.player).unwrap();
                    move_coord((position.x, position.y), keycode_to_direction(key))
                };

                let mut attacked = false;

                if let Some(target) = get_npc_at(world, &Position { x, y }) {
                    ecs.set_component_delayed(&world.player, Attacking { target: target });
                    attacked = true;
                }

                if world.level.is_passable(x, y) && !attacked {
                    let mut position_components =
                        ecs.get_components::<Position>().unwrap().write().unwrap();
                    let mut position = position_components.get_mut(&world.player).unwrap();
                    position.x = x;
                    position.y = y;
                }

                if attacked {
                    world.out_of_combat_turns = 0;
                }
            } else {
                match key {
                    Keycode::G => {
                        let mut picked_up = false;

                        let mut position_components =
                            ecs.get_components::<Position>().unwrap().write().unwrap();

                        if inventory.items.len() == inventory.cap {
                            log.push(format!("You are carrying too much!"));
                        } else {
                            for component in item_components.storage.iter() {
                                if let Some((entity, item)) = component {
                                    if ecs.is_alive(entity) {
                                        let position =
                                            position_components.get(&world.player).unwrap();
                                        let item_pos = position_components.get(entity).unwrap();

                                        if item_pos.x == position.x
                                            && item_pos.y == position.y
                                            && inventory.items.len() < inventory.cap
                                        {
                                            log.push(format!("You pick up the {}", item.name()));
                                            inventory.items.push(item.clone());
                                            ecs.destroy_entity(entity);
                                            picked_up = true;

                                            inventory.items.sort_by(|x, y| x.name().cmp(y.name()));

                                            break;
                                        }
                                    }
                                }
                            }
                        }

                        if !picked_up {
                            log.push(format!("There is nothing to pick up!"));
                        }
                    }
                    Keycode::Less => {
                        let mut position_components =
                            ecs.get_components::<Position>().unwrap().write().unwrap();
                        let position = position_components.get(&world.player).unwrap();

                        if world.level.get_tile(position.x, position.y) == Tile::Staircase {
                            world.depth += 1;
                        }
                    }
                    _ => (),
                }
            }
        }
    }

    fn post_run(&mut self, world: &mut World) {
        world.ecs.maintain();
    }
}

pub struct ApplyEffectsSystem {}

impl ApplyEffectsSystem {
    pub fn new() -> ApplyEffectsSystem {
        return ApplyEffectsSystem {};
    }
}

impl System for ApplyEffectsSystem {
    fn run(&mut self, world: &mut World) {
        let mut effect_components = world
            .ecs
            .get_components::<Effects>()
            .unwrap()
            .write()
            .unwrap();

        for component in effect_components.storage.iter_mut() {
            if let Some((entity, effects)) = component {
                for _i in 0..effects.effects.len() {
                    let mut effect = effects.effects.pop().unwrap();

                    let mut health_components = world
                        .ecs
                        .get_components::<Health>()
                        .unwrap()
                        .write()
                        .unwrap();

                    match effect {
                        Effect::Healing(amount) => {
                            if let Some(health) = health_components.get_mut(entity) {
                                health.hp += amount;
                                if health.hp > health.max_hp {
                                    health.hp = health.max_hp
                                };
                            }
                        }
                        // TODO: Stack poison instead of handling multiple Poison effects
                        Effect::Poison(amount) => {
                            if let Some(health) = health_components.get_mut(entity) {
                                health.hp -= amount;
                            }

                            if (amount > 1) {
                                effects.effects.push(Effect::Poison(amount - 1));
                            }
                        }
                        Effect::Fire(amount) => {
                            if let Some(health) = health_components.get_mut(entity) {
                                health.hp -= amount;
                            }
                        }
                    }
                }
            }
        }
    }

    fn post_run(&mut self, world: &mut World) {}
}

pub struct EntangleSystem {}

impl EntangleSystem {
    pub fn new() -> EntangleSystem {
        return EntangleSystem {};
    }
}

impl System for EntangleSystem {
    fn run(&mut self, world: &mut World) {
        let web_components = world.ecs.get_components::<Web>().unwrap().read().unwrap();
        let mut entangled_components = world
            .ecs
            .get_components::<Entangled>()
            .unwrap()
            .write()
            .unwrap();

        let mut entangle_resistant_components = world
            .ecs
            .get_components::<EntangleResistant>()
            .unwrap()
            .write()
            .unwrap();

        let position_components = world
            .ecs
            .get_components::<Position>()
            .unwrap()
            .read()
            .unwrap();

        let player_pos = position_components.get(&world.player).unwrap();

        for component in web_components.storage.iter() {
            if let Some((entity, web)) = component {
                if world.ecs.is_alive(entity) {
                    let pos = position_components.get(entity).unwrap();

                    if let Some(npc) = get_npc_at(&world, pos) {
                        if let None = entangle_resistant_components.get(&npc) {
                            world.ecs.set_component_delayed(
                                &npc,
                                Entangled {
                                    turns: lib_dice::roll_from_str("1d4 + 1").unwrap(),
                                },
                            );

                            world.ecs.destroy_entity(entity);
                        }
                    } else if player_pos == pos {
                        if let None = entangle_resistant_components.get(&world.player) {
                            world.ecs.set_component_delayed(
                                &world.player,
                                Entangled {
                                    turns: lib_dice::roll_from_str("1d4 + 1").unwrap(),
                                },
                            );
                            world.ecs.destroy_entity(entity);
                        }
                    }
                }
            }
        }

        for component in entangled_components.storage.iter_mut() {
            if let Some((entity, entangled)) = component {
                if world.ecs.is_alive(entity) {
                    entangled.turns -= 1;

                    if entangled.turns <= 0 {
                        world.ecs.remove_component_delayed::<Entangled>(entity);
                    }
                }
            }
        }

        for component in entangle_resistant_components.storage.iter_mut() {
            if let Some((entity, entangle_resistant)) = component {
                if world.ecs.is_alive(entity) {
                    if !entangle_resistant.permanent {
                        entangle_resistant.turns -= 1;

                        if entangle_resistant.turns <= 0 {
                            world
                                .ecs
                                .remove_component_delayed::<EntangleResistant>(entity);
                        }
                    }
                }
            }
        }
    }

    fn post_run(&mut self, world: &mut World) {
        world.ecs.maintain();
    }
}

pub struct FireSystem {}

impl FireSystem {
    pub fn new() -> FireSystem {
        return FireSystem {};
    }
}

impl System for FireSystem {
    fn run(&mut self, world: &mut World) {
        let mut fire_components = world.ecs.get_components::<Fire>().unwrap().write().unwrap();

        let mut fire_resistant_components = world
            .ecs
            .get_components::<FireResistant>()
            .unwrap()
            .write()
            .unwrap();

        let mut effects_components = world
            .ecs
            .get_components::<Effects>()
            .unwrap()
            .write()
            .unwrap();

        let position_components = world
            .ecs
            .get_components::<Position>()
            .unwrap()
            .read()
            .unwrap();

        let player_pos = position_components.get(&world.player).unwrap();

        for component in fire_components.storage.iter_mut() {
            if let Some((entity, fire)) = component {
                if world.ecs.is_alive(entity) {
                    fire.lifetime -= 1;

                    if fire.lifetime <= 0 {
                        world.ecs.destroy_entity(entity);
                    }

                    let pos = position_components.get(entity).unwrap();

                    if let Some(npc) = get_npc_at(&world, pos) {
                        if let None = fire_resistant_components.get(&npc) {
                            let mut target_effects = effects_components.get_mut(&npc);

                            // Target might not have an effects list
                            if target_effects.is_some() {
                                let mut target_effects = target_effects.unwrap();

                                // Oh boy, this will take some explaining:
                                // We attempt to find a previous Poison effect
                                // and increment it, rather than creating a new
                                // Poison effect
                                if let Some(Effect::Fire(value)) =
                                    target_effects.effects.iter_mut().find(|x| {
                                        if let Effect::Fire(value) = x {
                                            true
                                        } else {
                                            false
                                        }
                                    })
                                {
                                    *value += 5;
                                } else {
                                    // We couldn't find a previous Poison effect, so
                                    // we create a new one
                                    target_effects.effects.push(Effect::Fire(5));
                                }
                            } else {
                                // No effects list found, initialize a new one
                                world.ecs.set_component_delayed(
                                    &npc,
                                    Effects {
                                        effects: vec![Effect::Fire(5)],
                                    },
                                );
                            }
                        }
                    } else if player_pos == pos {
                        if let None = fire_resistant_components.get(&world.player) {
                            let mut target_effects = effects_components.get_mut(&world.player);

                            // Target might not have an effects list
                            if target_effects.is_some() {
                                let mut target_effects = target_effects.unwrap();

                                // Oh boy, this will take some explaining:
                                // We attempt to find a previous Poison effect
                                // and increment it, rather than creating a new
                                // Poison effect
                                if let Some(Effect::Fire(value)) =
                                    target_effects.effects.iter_mut().find(|x| {
                                        if let Effect::Fire(value) = x {
                                            true
                                        } else {
                                            false
                                        }
                                    })
                                {
                                    *value += 5;
                                } else {
                                    // We couldn't find a previous Poison effect, so
                                    // we create a new one
                                    target_effects.effects.push(Effect::Fire(5));
                                }
                            } else {
                                // No effects list found, initialize a new one
                                world.ecs.set_component_delayed(
                                    &world.player,
                                    Effects {
                                        effects: vec![Effect::Fire(5)],
                                    },
                                );
                            }
                        }
                    }
                }
            }
        }

        for component in fire_resistant_components.storage.iter_mut() {
            if let Some((entity, fire_resistant)) = component {
                if world.ecs.is_alive(entity) {
                    if !fire_resistant.permanent {
                        fire_resistant.turns -= 1;

                        if fire_resistant.turns <= 0 {
                            world.ecs.remove_component_delayed::<FireResistant>(entity);
                        }
                    }
                }
            }
        }
    }

    fn post_run(&mut self, world: &mut World) {
        world.ecs.maintain();
    }
}

pub fn bfs_pathfinder(world: &World, start: &Position, end: &Position) -> (Direction, i32) {
    let mut visited: HashSet<(i32, i32)> = HashSet::new();
    let mut to_visit: VecDeque<(i32, i32)> = VecDeque::new();
    let mut path: HashMap<(i32, i32), Direction> = HashMap::new();

    let begin = (start.x, start.y);
    let target = (end.x, end.y);

    to_visit.push_back(begin);
    path.insert(begin, Direction::Stop);

    //println!("Beginning path find...");

    while !to_visit.is_empty() {
        let (x, y) = to_visit.pop_front().unwrap();

        if (x, y) == target {
            //println!("Found path, backtracing...");

            let mut prev_dir = path.get(&(x, y)).unwrap().clone();
            let mut current_tile = (x, y);

            let mut length = 0;

            loop {
                let (cx, cy) = current_tile;
                length += 1;
                match path.get(&current_tile).unwrap() {
                    Direction::Left => {
                        current_tile = (cx + 1, cy);
                        prev_dir = Direction::Left;
                    }
                    Direction::Right => {
                        current_tile = (cx - 1, cy);
                        prev_dir = Direction::Right;
                    }
                    Direction::Up => {
                        current_tile = (cx, cy + 1);
                        prev_dir = Direction::Up;
                    }
                    Direction::Down => {
                        current_tile = (cx, cy - 1);
                        prev_dir = Direction::Down;
                    }
                    _ => break,
                }
            }

            return (prev_dir, length);
        }

        let directions = direction_priorities((x, y), target);

        for i in 0..directions.len() {
            let dir = directions[i];

            let (cx, cy) = move_coord((x, y), dir);

            if !visited.contains(&(cx, cy))
                && !to_visit.contains(&(cx, cy))
                && world.level.is_passable(cx, cy)
            {
                to_visit.push_back((cx, cy));
                path.insert((cx, cy), dir);
            }
        }

        visited.insert((x, y));
    }

    return (Direction::Stop, 0);
}

use std::cmp::Reverse;

fn invert(direction: Direction) -> Direction {
    match direction {
        Direction::Up => Direction::Down,
        Direction::Down => Direction::Up,
        Direction::Left => Direction::Right,
        Direction::Right => Direction::Left,
        Direction::Stop => Direction::Stop,
    }
}

fn surrounding(coord: (i32, i32)) -> [(Direction, (i32, i32)); 4] {
    let (x, y) = coord;

    return [
        (Direction::Left, (x - 1, y)),
        (Direction::Right, (x + 1, y)),
        (Direction::Up, (x, y - 1)),
        (Direction::Down, (x, y + 1)),
    ];
}

pub fn get_npc_at(world: &World, pos: &Position) -> Option<Entity> {
    let npc_components = world.ecs.get_components::<NPC>().unwrap().read().unwrap();
    let position_components = world
        .ecs
        .get_components::<Position>()
        .unwrap()
        .read()
        .unwrap();

    for component in npc_components.storage.iter() {
        if let Some((entity, npc)) = component {
            if world.ecs.is_alive(entity) {
                let npc_pos = position_components.get(entity).unwrap();

                if npc_pos == pos {
                    return Some(entity.clone());
                }
            }
        }
    }

    return None;
}

pub fn dijkstra_pathfinder(
    world: &World,
    start: &Position,
    target: &Position,
) -> (Vec<Direction>, i32) {
    let end = (start.x, start.y);
    let begin = (target.x, target.y);

    let mut dist: HashMap<(i32, i32), i32> = HashMap::new();
    let mut points = BinaryHeap::new();
    let mut direction: HashMap<(i32, i32), Direction> = HashMap::new();

    for y in 0..MAX_Y as i32 {
        for x in 0..MAX_X as i32 {
            dist.insert((x, y), 9999);
            direction.insert((x, y), Direction::Stop);
        }
    }

    dist.insert(begin, 0);
    points.push(Reverse((0, begin)));

    let npc_components = world.ecs.get_components::<NPC>().unwrap().read().unwrap();
    let position_components = world
        .ecs
        .get_components::<Position>()
        .unwrap()
        .read()
        .unwrap();

    while !points.is_empty() {
        let Reverse((weight, u)) = points.pop().unwrap();

        for (dir, v) in surrounding(u).iter() {
            let mut alt = *dist.get(&u).unwrap();
            alt += 1;

            if get_npc_at(world, &Position { x: v.0, y: v.1 }).is_some() {
                alt += 5;
            }

            if world.level.is_passable(v.0, v.1) && &alt < dist.get(&v).unwrap() {
                dist.insert(*v, alt);
                direction.insert(*v, invert(*dir));

                points.push(Reverse((alt, *v)));
            }
        }
    }

    let mut path = Vec::new();
    let mut dir = direction.get(&end).unwrap();
    let len = dist.get(&end).unwrap();

    let mut cur_x = target.x;
    let mut cur_y = target.y;

    loop {
        path.push(dir.clone());

        let new_coord = move_coord((cur_x, cur_y), *dir);

        if cur_x == new_coord.0 && cur_y == new_coord.1 {
            break;
        }

        cur_x = new_coord.0;
        cur_y = new_coord.1;

        dir = direction.get(&(cur_x, cur_y)).unwrap();
    }

    path.push(Direction::Stop);

    return (path, *len);
}

pub struct AttackResolutionSystem {}

impl AttackResolutionSystem {
    pub fn new() -> AttackResolutionSystem {
        return AttackResolutionSystem {};
    }
}

impl System for AttackResolutionSystem {
    fn run(&mut self, world: &mut World) {
        let ecs = &world.ecs;
        let mut log = world.log.borrow_mut();

        let name_components = world.ecs.get_components::<Name>().unwrap().read().unwrap();
        let attacking_components = world
            .ecs
            .get_components::<Attacking>()
            .unwrap()
            .read()
            .unwrap();
        let base_attack_components = world
            .ecs
            .get_components::<BaseAttack>()
            .unwrap()
            .read()
            .unwrap();
        let armed_components = world.ecs.get_components::<Armed>().unwrap().read().unwrap();
        let mut health_components = world
            .ecs
            .get_components::<Health>()
            .unwrap()
            .write()
            .unwrap();
        let mut effects_components = world
            .ecs
            .get_components::<Effects>()
            .unwrap()
            .write()
            .unwrap();

        let mut rng = thread_rng();

        attacking_components.storage.iter().for_each(|component| {
            if let Some((entity, attacking)) = component {
                if ecs.is_alive(entity) {
                    let base_attack = base_attack_components.get(entity).unwrap();

                    let armed = armed_components.get(entity);

                    let mut target_health = health_components.get_mut(&attacking.target).unwrap();
                    let attacker_name = name_components.get(entity).unwrap();
                    let target_name = name_components.get(&attacking.target).unwrap();

                    let attack_roll = lib_dice::roll_from_str("1d20").unwrap();

                    let ac = calculate_ac(ecs, &attacking.target);

                    if (attack_roll < ac) {
                        log.push(format!(
                            "{} attacks {} but does no damage",
                            attacker_name.name, target_name.name
                        ));
                        ecs.remove_component_delayed::<Attacking>(entity);

                        return;
                    }

                    let attack = if let Some(armed) = armed {
                        let weapon = &armed.weapon;

                        weapon.attack()
                    } else {
                        base_attack.attack.clone()
                    };

                    match attack {
                        Attack::Physical(dmg) => {
                            let actual_dmg = lib_dice::roll_from_str(dmg).unwrap();

                            log.push(format!(
                                "{} attacks {} for {} damage",
                                attacker_name.name, target_name.name, actual_dmg
                            ));
                            target_health.hp -= actual_dmg;
                        }
                        Attack::Poison(dmg) => {
                            let actual_dmg = lib_dice::roll_from_str(dmg).unwrap();

                            log.push(format!(
                                "{} poisons {} for {} damage",
                                attacker_name.name, target_name.name, actual_dmg
                            ));

                            let mut target_effects = effects_components.get_mut(&attacking.target);

                            // Target might not have an effects list
                            if target_effects.is_some() {
                                let mut target_effects = target_effects.unwrap();

                                // Oh boy, this will take some explaining:
                                // We attempt to find a previous Poison effect
                                // and increment it, rather than creating a new
                                // Poison effect
                                if let Some(Effect::Poison(value)) =
                                    target_effects.effects.iter_mut().find(|x| {
                                        if let Effect::Poison(value) = x {
                                            true
                                        } else {
                                            false
                                        }
                                    })
                                {
                                    *value += actual_dmg;
                                } else {
                                    // We couldn't find a previous Poison effect, so
                                    // we create a new one
                                    target_effects.effects.push(Effect::Poison(actual_dmg));
                                }
                            } else {
                                // No effects list found, initialize a new one
                                world.ecs.set_component_delayed(
                                    &attacking.target,
                                    Effects {
                                        effects: vec![Effect::Poison(actual_dmg)],
                                    },
                                );
                            }
                        }
                        Attack::Fire(dmg) => {
                            let actual_dmg = lib_dice::roll_from_str(dmg).unwrap();

                            log.push(format!(
                                "{} sets {} on fire {} damage",
                                attacker_name.name, target_name.name, actual_dmg
                            ));

                            let mut target_effects = effects_components.get_mut(&attacking.target);

                            // Target might not have an effects list
                            if target_effects.is_some() {
                                let mut target_effects = target_effects.unwrap();

                                // Oh boy, this will take some explaining:
                                // We attempt to find a previous Poison effect
                                // and increment it, rather than creating a new
                                // Poison effect
                                if let Some(Effect::Fire(value)) =
                                    target_effects.effects.iter_mut().find(|x| {
                                        if let Effect::Fire(value) = x {
                                            true
                                        } else {
                                            false
                                        }
                                    })
                                {
                                    *value += actual_dmg;
                                } else {
                                    // We couldn't find a previous Poison effect, so
                                    // we create a new one
                                    target_effects.effects.push(Effect::Fire(actual_dmg));
                                }
                            } else {
                                // No effects list found, initialize a new one
                                world.ecs.set_component_delayed(
                                    &attacking.target,
                                    Effects {
                                        effects: vec![Effect::Fire(actual_dmg)],
                                    },
                                );
                            }
                        }
                        _ => println!("AttackResolutionSystem: Unknown attack type."),
                    }

                    ecs.remove_component_delayed::<Attacking>(entity);
                }
            }
        });
    }

    fn post_run(&mut self, world: &mut World) {
        world.ecs.maintain();
    }
}

pub fn calculate_ac(ecs: &ECS, entity: &Entity) -> i32 {
    let mut sum_ac = 0;

    let shielded_components = ecs.get_components::<Shielded>().unwrap().read().unwrap();

    let armored_components = ecs.get_components::<Armored>().unwrap().read().unwrap();

    if let Some(shielded) = shielded_components.get(entity) {
        sum_ac += shielded.shield.ac();
    }

    if let Some(armored) = armored_components.get(entity) {
        sum_ac += armored.armor.ac();
    }

    return sum_ac;
}

pub struct GrimReaperSystem {}

impl GrimReaperSystem {
    pub fn new() -> GrimReaperSystem {
        return GrimReaperSystem {};
    }
}

impl System for GrimReaperSystem {
    fn run(&mut self, world: &mut World) {
        let npc_components = world.ecs.get_components::<NPC>().unwrap().read().unwrap();
        let health_components = world
            .ecs
            .get_components::<Health>()
            .unwrap()
            .read()
            .unwrap();
        let position_components = world
            .ecs
            .get_components::<Position>()
            .unwrap()
            .read()
            .unwrap();

        npc_components.storage.iter().for_each(|component| {
            if let Some((entity, _)) = component {
                if world.ecs.is_alive(entity) {
                    let health = health_components.get(entity).unwrap();

                    if health.hp <= 0 {
                        let armed_components =
                            world.ecs.get_components::<Armed>().unwrap().read().unwrap();
                        let shielded_components = world
                            .ecs
                            .get_components::<Shielded>()
                            .unwrap()
                            .read()
                            .unwrap();
                        let inventory_components = world
                            .ecs
                            .get_components::<Inventory>()
                            .unwrap()
                            .read()
                            .unwrap();
                        let pos = position_components.get(entity).unwrap();

                        if let Some(armed) = armed_components.get(entity) {
                            let entity = world.ecs.create_entity();

                            world.ecs.set_component_delayed(&entity, pos.clone());
                            world
                                .ecs
                                .set_component_delayed(&entity, Item::Weapon(armed.weapon.clone()));
                            world.ecs.set_component_delayed(
                                &entity,
                                Renderable {
                                    sprite: String::from(armed.weapon.sprite()),
                                    item: true,
                                },
                            );
                        }

                        if let Some(shielded) = shielded_components.get(entity) {
                            let entity = world.ecs.create_entity();

                            world.ecs.set_component_delayed(&entity, pos.clone());
                            world.ecs.set_component_delayed(
                                &entity,
                                Item::Shield(shielded.shield.clone()),
                            );
                            world.ecs.set_component_delayed(
                                &entity,
                                Renderable {
                                    sprite: String::from(shielded.shield.sprite()),
                                    item: true,
                                },
                            );
                        }

                        if let Some(inventory) = inventory_components.get(entity) {
                            for item in &inventory.items {
                                let entity = world.ecs.create_entity();

                                world.ecs.set_component_delayed(&entity, pos.clone());
                                world.ecs.set_component_delayed(&entity, item.clone());
                                world.ecs.set_component_delayed(
                                    &entity,
                                    Renderable {
                                        sprite: String::from(item.sprite()),
                                        item: true,
                                    },
                                );
                            }
                        }

                        world.ecs.destroy_entity(entity);
                    }
                }
            }
        });
    }

    fn post_run(&mut self, world: &mut World) {
        world.ecs.maintain();
    }
}

pub struct NPCSystem {}

impl NPCSystem {
    pub fn new() -> NPCSystem {
        return NPCSystem {};
    }
}

impl System for NPCSystem {
    fn run(&mut self, world: &mut World) {
        let npc_components = world.ecs.get_components::<NPC>().unwrap().read().unwrap();
        let entangled_components = world
            .ecs
            .get_components::<Entangled>()
            .unwrap()
            .read()
            .unwrap();

        npc_components.storage.iter().for_each(|component| {
            if let Some((entity, npc)) = component {
                if world.ecs.is_alive(entity) {
                    let action = npc.brain.next_action(&world, entity);

                    let mut position_components = world
                        .ecs
                        .get_components::<Position>()
                        .unwrap()
                        .write()
                        .unwrap();
                    let mut attacking_components = world
                        .ecs
                        .get_components::<Attacking>()
                        .unwrap()
                        .write()
                        .unwrap();
                    let mut new_pos = {
                        let position = position_components.get(entity).unwrap();

                        Position {
                            x: position.x,
                            y: position.y,
                        }
                    };

                    match action {
                        Action::MoveUp => new_pos.y -= 1,
                        Action::MoveDown => new_pos.y += 1,
                        Action::MoveLeft => new_pos.x -= 1,
                        Action::MoveRight => new_pos.x += 1,
                        Action::Attack => world.ecs.set_component_delayed(
                            entity,
                            Attacking {
                                target: world.player.clone(),
                            },
                        ),
                        _ => (),
                    }

                    let tile_occupied = {
                        let mut found = false;

                        for component in npc_components.storage.iter() {
                            if let Some((entity, component)) = component {
                                if world.ecs.is_alive(entity) {
                                    let pos = position_components.get(entity).unwrap();

                                    if pos.x == new_pos.x && pos.y == new_pos.y {
                                        found = true;
                                        break;
                                    }
                                }
                            }
                        }

                        found
                    };

                    let mut position = position_components.get_mut(entity).unwrap();

                    if let None = entangled_components.get(entity) {
                        if world.level.is_passable(new_pos.x, new_pos.y) && !tile_occupied {
                            position.x = new_pos.x;
                            position.y = new_pos.y;
                        }
                    }
                }
            }
        });
    }

    fn post_run(&mut self, world: &mut World) {
        world.ecs.maintain();
    }
}
