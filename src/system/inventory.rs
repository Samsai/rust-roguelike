extern crate sdl2;

use sdl2::keyboard::Keycode;
use sdl2::pixels::Color;
use sdl2::rect::Rect;

use super::System;

use World;

use component::*;
use item::*;
use helper::*;

#[derive(Clone)]
pub struct InventoryIndex {
    pub index: usize,
}

pub struct RenderInventorySystem {}

impl RenderInventorySystem {
    pub fn new() -> RenderInventorySystem {
        return RenderInventorySystem {};
    }
}

impl System for RenderInventorySystem {
    fn run(&mut self, world: &mut World) {
        world
            .graphics
            .canvas
            .set_draw_color(Color::RGB(255, 255, 255));
        world.graphics.canvas.fill_rect(Rect::new(45, 45, 410, 410));
        world
            .graphics
            .canvas
            .set_draw_color(Color::RGB(0, 100, 100));
        world.graphics.canvas.fill_rect(Rect::new(50, 50, 400, 400));
        world.graphics.canvas.set_draw_color(Color::RGB(0, 0, 0));

        let selection_index = world
            .resources
            .get::<InventoryIndex>()
            .unwrap_or(&InventoryIndex { index: 0 });

        let mut y = 50;

        let inventory_components = world
            .ecs
            .get_components::<Inventory>()
            .unwrap()
            .read()
            .unwrap();

        let inventory = inventory_components.get(&world.player).unwrap();

        for index in 0..inventory.items.len() {
            let item = &inventory.items[index];

            world
                .graphics
                .draw_sprite_abs(&String::from(item.sprite()), 70, y);

            if index == selection_index.index {
                world.graphics.draw_sprite_abs("selection_arrow.png", 50, y);
                world.graphics.draw_text_rgb(
                    &String::from(item.name()),
                    (90, y),
                    Color::RGB(255, 255, 0),
                );
            } else {
                world
                    .graphics
                    .draw_text(&String::from(item.name()), (90, y));
            }

            y += 20;
        }
    }

    fn post_run(&mut self, world: &mut World) {}
}

pub struct PlayerInventorySystem {}

impl PlayerInventorySystem {
    pub fn new() -> PlayerInventorySystem {
        return PlayerInventorySystem {};
    }
}

impl System for PlayerInventorySystem {
    fn run(&mut self, world: &mut World) {
        if world.resources.get_mut::<InventoryIndex>().is_none() {
            world.resources.insert(InventoryIndex { index: 0 });
        }

        let mut index = world.resources.get_mut::<InventoryIndex>().unwrap().clone();

        let mut inventory_components = world
            .ecs
            .get_components::<Inventory>()
            .unwrap()
            .write()
            .unwrap();

        let mut inventory = inventory_components.get_mut(&world.player).unwrap();

        if let Some(key) = world.key {
            if is_keycode_direction(key) {
                let dir = keycode_to_direction(key);

                match dir {
                    Direction::Up => {
                        if index.index > 0 {
                            index.index -= 1;
                        } else {
                            index.index = inventory.items.len() - 1;
                        }
                    },
                    Direction::Down => {
                        if index.index < inventory.items.len() - 1 {
                            index.index += 1;
                        } else {
                            index.index = 0;
                        }
                    }
                    _ => {},
                }
            }

            else {
                match key {
                    Keycode::Return => {
                        world.skip_turn = true;

                        if index.index < inventory.items.len() {
                            let item = inventory.items.swap_remove(index.index);

                            match item {
                                Item::Consumable(consumable) => {
                                    consumable.consume(world, &world.player);
                                }
                                Item::Weapon(weapon) => {
                                    let mut armed_components =
                                        world.ecs.get_components::<Armed>().unwrap().read().unwrap();

                                    if let Some(armed) = armed_components.get(&world.player) {
                                        inventory.items.push(Item::Weapon(armed.weapon.clone()));
                                    }

                                    world
                                        .ecs
                                        .set_component_delayed(&world.player, Armed { weapon: weapon });
                                }
                                Item::Shield(shield) => {
                                    let mut shielded_components = world
                                        .ecs
                                        .get_components::<Shielded>()
                                        .unwrap()
                                        .read()
                                        .unwrap();

                                    if let Some(shielded) = shielded_components.get(&world.player) {
                                        inventory.items.push(Item::Shield(shielded.shield.clone()));
                                    }

                                    world.ecs.set_component_delayed(
                                        &world.player,
                                        Shielded { shield: shield },
                                    );
                                }
                                Item::Armor(armor) => {
                                    let mut armored_components = world
                                        .ecs
                                        .get_components::<Armored>()
                                        .unwrap()
                                        .read()
                                        .unwrap();

                                    if let Some(armored) = armored_components.get(&world.player) {
                                        inventory.items.push(Item::Armor(armored.armor.clone()));
                                    }

                                    world
                                        .ecs
                                        .set_component_delayed(&world.player, Armored { armor: armor });
                                }
                            }
                        }

                        inventory.items.sort_by(|x, y| x.name().cmp(y.name()));
                        index.index = 0;
                    },
                    Keycode::D => {
                        if index.index < inventory.items.len() {
                            let item = inventory.items.swap_remove(index.index);

                            let position_components = world
                                .ecs
                                .get_components::<Position>()
                                .unwrap()
                                .read()
                                .unwrap();
                            let pos = position_components.get(&world.player).unwrap();

                            let entity = world.ecs.create_entity();

                            world.ecs.set_component_delayed(&entity, pos.clone());
                            world.ecs.set_component_delayed(&entity, item.clone());
                            world.ecs.set_component_delayed(
                                &entity,
                                Renderable {
                                    sprite: String::from(item.sprite()),
                                    item: true,
                                },
                            );
                        }
                        index.index = 0;
                        inventory.items.sort_by(|x, y| x.name().cmp(y.name()));
                    },
                    _ => (),
                }
            }
        }


        let mut current_index = world.resources.get_mut::<InventoryIndex>().unwrap();
        current_index.index = index.index;
    }

    fn post_run(&mut self, world: &mut World) {
        world.ecs.maintain();
    }
}
