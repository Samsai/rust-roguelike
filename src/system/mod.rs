extern crate rand;
extern crate sdl2;

use sdl2::keyboard::Keycode;
use sdl2::pixels::Color;
use sdl2::rect::Rect;

use rand::*;

use std::collections::HashMap;
use std::collections::HashSet;
use std::collections::VecDeque;

use component::*;
use entity::*;
use level::*;
use *;

use helper::*;

pub mod dungeon;
pub mod gameover;
pub mod inspect_item;
pub mod inventory;
pub mod mainmenu;
pub mod victory;

pub trait System {
    fn execute(&mut self, world: &mut World) {
        self.run(world);
        self.post_run(world);
    }
    fn run(&mut self, world: &mut World);
    fn post_run(&mut self, world: &mut World);
}

pub struct ClusterbombSystem {}

impl System for ClusterbombSystem {
    fn run(&mut self, world: &mut World) {
        let ecs = &mut world.ecs;

        let mut clusterbomb_components = ecs
            .get_components::<Clusterbomb>()
            .unwrap()
            .write()
            .unwrap();

        let mut position_components = ecs.get_components::<Position>().unwrap().write().unwrap();

        clusterbomb_components
            .storage
            .iter_mut()
            .for_each(|component| {
                if let Some((entity, clusterbomb)) = component {
                    if ecs.is_alive(entity) {
                        let position = position_components.get(entity).unwrap();
                        clusterbomb.timer -= 1;

                        if (clusterbomb.timer <= 0) {
                            println!("BOOM!");
                            let mut rng = thread_rng();

                            for i in 0..clusterbomb.submunitions {
                                let x_delta = rng.gen_range(-5, 5);
                                let y_delta = rng.gen_range(-5, 5);

                                let mut sub_bomb = ecs.create_entity();

                                ecs.set_component_delayed(
                                    &sub_bomb,
                                    Position {
                                        x: position.x + x_delta,
                                        y: position.y + y_delta,
                                    },
                                );

                                ecs.set_component_delayed(
                                    &sub_bomb,
                                    Clusterbomb {
                                        timer: 1,
                                        submunitions: clusterbomb.submunitions - 1,
                                    },
                                );
                                ecs.set_component_delayed(
                                    &sub_bomb,
                                    Renderable {
                                        sprite: String::from("red_potion.png"),
                                        item: false,
                                    },
                                );
                            }

                            ecs.destroy_entity(&entity);
                        }
                    }
                }
            });
    }

    fn post_run(&mut self, world: &mut World) {
        world.ecs.maintain();
    }
}
