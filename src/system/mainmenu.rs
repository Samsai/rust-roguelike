use sdl2::keyboard::Keycode;
use sdl2::pixels::Color;
use sdl2::rect::Rect;

use system::System;
use helper::*;

use World;

pub static MENUOPTIONS: &[&str] = &["New Game", "Quit"];

pub struct MainMenuIndex {
    pub index: usize,
}

pub struct RenderMainMenuSystem {}

impl RenderMainMenuSystem {
    pub fn new() -> RenderMainMenuSystem {
        return RenderMainMenuSystem {};
    }
}

impl System for RenderMainMenuSystem {
    fn run(&mut self, world: &mut World) {
        let mut graphics = &mut world.graphics;

        graphics.canvas.set_draw_color(Color::RGB(255, 255, 255));
        let background = graphics.scaled_rect(0.24, 0.24, 0.42, 0.42);
        graphics.canvas.fill_rect(background);

        graphics.canvas.set_draw_color(Color::RGB(0, 100, 100));
        let background = graphics.scaled_rect(0.25, 0.25, 0.4, 0.4);
        graphics.canvas.fill_rect(background);
        graphics.canvas.set_draw_color(Color::RGB(0, 0, 0));

        let selection_index = world
            .resources
            .get::<MainMenuIndex>()
            .unwrap_or(&MainMenuIndex { index: 0 });

        let (x, y) = graphics.scaled_pos(0.4, 0.3);

        graphics.draw_bold_text(&String::from("Aecidium"), (x, y));

        let mut y_offset = 0.4;

        for index in 0..MENUOPTIONS.len() {
            let (x, y) = graphics.scaled_pos(0.4, y_offset);

            let item = MENUOPTIONS[index];

            if index == selection_index.index {
                graphics.draw_text_rgb(
                    &String::from(MENUOPTIONS[index]),
                    (x, y),
                    Color::RGB(255, 255, 0),
                );
            } else {
                graphics.draw_text(&String::from(MENUOPTIONS[index]), (x, y));
            }

            y_offset += 0.1;
        }
    }

    fn post_run(&mut self, world: &mut World) {}
}

pub struct MainMenuInputSystem {}

impl MainMenuInputSystem {
    pub fn new() -> MainMenuInputSystem {
        return MainMenuInputSystem {};
    }
}

impl System for MainMenuInputSystem {
    fn run(&mut self, world: &mut World) {
        if world.resources.get_mut::<MainMenuIndex>().is_none() {
            world.resources.insert(MainMenuIndex { index: 0 });
        }

        if let Some(selection) = world.resources.get_mut::<MainMenuIndex>() {
            if let Some(key) = world.key {

                if is_keycode_direction(key) {
                    let dir = keycode_to_direction(key);

                    match dir {
                        Direction::Up => {
                            if selection.index > 0 {
                                selection.index -= 1;
                            }
                        },
                        Direction::Down => {
                            if selection.index < MENUOPTIONS.len() - 1 {
                                selection.index += 1;
                            }
                        },
                        _ => (),
                    }
                }
            }
        }
    }

    fn post_run(&mut self, world: &mut World) {}
}
