use component::*;
use system::inventory::InventoryIndex;
use system::*;

pub struct RenderInspectItemSystem {}

impl RenderInspectItemSystem {
    pub fn new() -> RenderInspectItemSystem {
        return RenderInspectItemSystem {};
    }
}

impl System for RenderInspectItemSystem {
    fn run(&mut self, world: &mut World) {
        world
            .graphics
            .canvas
            .set_draw_color(Color::RGB(255, 255, 255));
        world.graphics.canvas.fill_rect(Rect::new(45, 45, 410, 410));
        world
            .graphics
            .canvas
            .set_draw_color(Color::RGB(0, 100, 100));
        world.graphics.canvas.fill_rect(Rect::new(50, 50, 400, 400));
        world.graphics.canvas.set_draw_color(Color::RGB(0, 0, 0));

        let selection_index = world
            .resources
            .get::<InventoryIndex>()
            .unwrap_or(&InventoryIndex { index: 0 });

        let mut y = 50;

        let inventory_components = world
            .ecs
            .get_components::<Inventory>()
            .unwrap()
            .read()
            .unwrap();

        let inventory = inventory_components.get(&world.player).unwrap();

        let item = &inventory.items[selection_index.index];

        world
            .graphics
            .draw_text(&String::from(item.name()), (90, 50));

        match item {
            Item::Armor(armor) => {
                world
                    .graphics
                    .draw_text(&format!("AC: {}", armor.ac()), (90, 70));
            }
            Item::Consumable(consumable) => {}
            Item::Shield(shield) => {
                world
                    .graphics
                    .draw_text(&format!("AC: {}", shield.ac()), (90, 70));
            }
            Item::Weapon(weapon) => {
                match weapon.attack() {
                    Attack::Physical(dmg) => world
                        .graphics
                        .draw_text(&format!("Physical: {}", dmg), (90, 70)),
                    Attack::Poison(dmg) => world
                        .graphics
                        .draw_text(&format!("Poison: {}", dmg), (90, 70)),
                    Attack::Fire(dmg) => world
                        .graphics
                        .draw_text(&format!("Fire: {}", dmg), (90, 70)),
                };
            }
        }

        world
            .graphics
            .draw_text_wrapping(&String::from(item.description()), (90, 90), 350);
    }

    fn post_run(&mut self, world: &mut World) {}
}
