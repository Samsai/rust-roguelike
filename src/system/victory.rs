extern crate sdl2;

use sdl2::pixels::Color;

use super::System;

use World;

pub struct RenderVictorySystem {}

impl RenderVictorySystem {
    pub fn new() -> RenderVictorySystem {
        return RenderVictorySystem {};
    }
}

impl System for RenderVictorySystem {
    fn run(&mut self, world: &mut World) {
        world.graphics.canvas.set_draw_color(Color::RGB(0, 0, 0));
        world.graphics.canvas.clear();

        let center = world.graphics.scaled_pos(0.45, 0.4);

        world
            .graphics
            .draw_text_rgb(&String::from("YOU WIN"), center, Color::RGB(255, 0, 0));
    }

    fn post_run(&mut self, world: &mut World) {}
}
