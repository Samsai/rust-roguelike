use std::rc::Rc;

use creatures::*;
use entity::Entity;
use helper::*;
use item::*;

pub trait Component {}

#[derive(Clone, PartialEq)]
pub struct Position {
    pub x: i32,
    pub y: i32,
}

impl Component for Position {}

#[derive(Clone)]
pub struct Renderable {
    pub sprite: String,
    pub item: bool,
}

impl Component for Renderable {}

#[derive(Clone)]
pub struct Health {
    pub hp: i32,
    pub max_hp: i32,
}

#[derive(Clone)]
pub struct Name {
    pub name: String,
}

#[derive(Clone)]
pub struct KeyboardControlled {}

#[derive(Clone)]
pub struct Clusterbomb {
    pub timer: i32,
    pub submunitions: i32,
}

impl Component for Clusterbomb {}

#[derive(Clone)]
pub struct NPC {
    pub brain: Rc<Brain>,
}

impl Component for NPC {}

#[derive(Clone)]
pub struct Effects {
    pub effects: Vec<Effect>,
}

impl Component for Effects {}

#[derive(Clone)]
pub struct Attacking {
    pub target: Entity,
}

impl Component for Attacking {}

#[derive(Clone)]
pub struct Entangled {
    pub turns: i32,
}

impl Component for Entangled {}

#[derive(Clone)]
pub struct EntangleResistant {
    pub turns: i32,
    pub permanent: bool,
}

impl Component for EntangleResistant {}

#[derive(Clone)]
pub struct Web {}

impl Component for Web {}

#[derive(Clone)]
pub struct FireResistant {
    pub turns: i32,
    pub permanent: bool,
}

impl Component for FireResistant {}

#[derive(Clone)]
pub struct Fire {
    pub lifetime: i32,
}

impl Component for Fire {}

#[derive(Clone)]
pub struct Armed {
    pub weapon: Rc<Weapon>,
}

impl Component for Armed {}

#[derive(Clone)]
pub struct BaseAttack<'a> {
    pub attack: Attack<'a>,
}

impl<'a> Component for BaseAttack<'a> {}

#[derive(Clone)]
pub struct Shielded {
    pub shield: Rc<Shield>,
}

impl Component for Shielded {}

#[derive(Clone)]
pub struct Armored {
    pub armor: Rc<Armor>,
}

impl Component for Armor {}

#[derive(Clone)]
pub struct Inventory {
    pub items: Vec<Item>,
    pub cap: usize,
}

impl Component for Inventory {}

#[derive(Clone)]
pub struct Vision {
    pub range: i32,
    pub max_range: i32,
}

impl Component for Vision {}
