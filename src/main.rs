/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

extern crate anymap;
extern crate rand;
extern crate sdl2;
extern crate sdl2_sys;

use std::cell::Cell;
use std::cell::RefCell;
use std::path::Path;
use std::rc::Rc;

use sdl2::event::Event;
use sdl2::image::LoadTexture;
use sdl2::keyboard::Keycode;
use sdl2::pixels::Color;
use sdl2::rect::Rect;
use sdl2::render::TextureQuery;

use sdl2_sys::SDL_RenderSetIntegerScale;
use sdl2_sys::SDL_SetHint;
use sdl2_sys::SDL_HINT_RENDER_SCALE_QUALITY;

use anymap::AnyMap;

use rand::rngs::StdRng;
use rand::*;

use std::thread;
use std::time;

mod level;
use level::*;

mod statemanager;
use statemanager::*;

mod states;
use states::*;

mod resourcemanager;
use resourcemanager::FontManager;
use resourcemanager::TextureManager;

mod log;
use log::*;

mod helper;

mod entity;
use entity::*;

mod component;
use component::*;

mod system;
use system::*;

mod creatures;
use creatures::*;

mod item;
use item::*;

fn main() {
    let result = init();

    match result {
        Err(e) => println!("Error occured: {}", e),
        Ok(e) => println!("{}", e),
    }
}

pub struct Graphics<'a, 'b> {
    canvas: sdl2::render::Canvas<sdl2::video::Window>,
    res_w: usize,
    res_h: usize,
    texture_manager: TextureManager<'a>,
    font_manager: FontManager<'a, 'b>,
}

impl<'a, 'b> Graphics<'a, 'b> {
    fn draw_text(&mut self, text: &str, (x, y): (i32, i32)) -> Result<TextureQuery, String> {
        let header_texture = self.font_manager.render(&text);
        let text_info = header_texture.query();

        self.canvas.copy(
            &header_texture,
            None,
            Rect::new(x, y, text_info.width, text_info.height),
        )?;

        return Ok(text_info);
    }

    fn draw_text_wrapping(
        &mut self,
        text: &str,
        (x, y): (i32, i32),
        len: u32,
    ) -> Result<TextureQuery, String> {
        let header_texture = self.font_manager.render_wrapped(&text, len);
        let text_info = header_texture.query();

        self.canvas.copy(
            &header_texture,
            None,
            Rect::new(x, y, text_info.width, text_info.height),
        )?;

        return Ok(text_info);
    }

    fn draw_text_rgb(
        &mut self,
        text: &str,
        (x, y): (i32, i32),
        color: Color,
    ) -> Result<TextureQuery, String> {
        let header_texture = self.font_manager.render_rgb(&text, color);
        let text_info = header_texture.query();

        self.canvas.copy(
            &header_texture,
            None,
            Rect::new(x, y, text_info.width, text_info.height),
        )?;

        return Ok(text_info);
    }

    fn draw_bold_text(&mut self, text: &str, (x, y): (i32, i32)) -> Result<TextureQuery, String> {
        let header_texture = self.font_manager.render_bold(&text);
        let text_info = header_texture.query();

        self.canvas.copy(
            &header_texture,
            None,
            Rect::new(x, y, text_info.width, text_info.height),
        )?;

        return Ok(text_info);
    }

    fn draw_sprite(&mut self, sprite_string: &str, x: i32, y: i32) {
        let sprite = self.texture_manager.load(sprite_string);

        let tex_info = sprite.query();

        self.canvas
            .copy(&sprite, None, Rect::new(x * 20, y * 20, 20, 20));
    }

    fn draw_sprite_abs(&mut self, sprite_string: &str, x: i32, y: i32) {
        let sprite = self.texture_manager.load(sprite_string);

        let tex_info = sprite.query();

        self.canvas.copy(&sprite, None, Rect::new(x, y, 20, 20));
    }

    fn resolution(&self) -> (u32, u32) {
        return self.canvas.logical_size();
    }

    fn scaled_rect(&self, x: f32, y: f32, w: f32, h: f32) -> Rect {
        let (res_w, res_h) = self.resolution();

        let true_x = (res_w as f32 * x) as i32;
        let true_y = (res_h as f32 * y) as i32;
        let true_w = (res_w as f32 * w) as u32;
        let true_h = (res_h as f32 * h) as u32;

        return Rect::new(true_x, true_y, true_w, true_h);
    }

    fn scaled_pos(&self, x: f32, y: f32) -> (i32, i32) {
        let (res_w, res_h) = self.resolution();

        let true_x = (res_w as f32 * x) as i32;
        let true_y = (res_h as f32 * y) as i32;

        return (true_x, true_y);
    }
}

pub struct World<'a, 'b> {
    pub ecs: ECS,
    pub resources: AnyMap,
    pub rng: StdRng,
    pub level: Level,
    pub depth: u32,
    pub skip_turn: bool,
    pub turns: u32,
    pub out_of_combat_turns: u32,
    pub graphics: Graphics<'a, 'b>,
    pub key: Option<Keycode>,
    pub player: Entity,
    pub log: RefCell<Vec<String>>,
    pub tiles_memory: RefCell<Vec<(i32, i32)>>,
    pub running: bool,
    pub victory: Cell<bool>,
}

impl<'a, 'b> World<'a, 'b> {
    fn new(graphics: Graphics<'a, 'b>) -> World<'a, 'b> {
        let mut ecs = ECS::new();

        let mut player = ecs.create_entity();
        let mut tiles_memory = RefCell::new(Vec::new());

        return World {
            ecs,
            resources: AnyMap::new(),
            rng: StdRng::from_entropy(),
            level: Level::new(),
            depth: 1,
            turns: 0,
            out_of_combat_turns: 0,
            skip_turn: false,
            graphics,
            key: None,
            player,
            log: RefCell::new(Vec::new()),
            tiles_memory,
            running: true,
            victory: Cell::new(false),
        };
    }
}

fn init() -> Result<String, String> {
    let sdl_context = sdl2::init().unwrap();
    let video_subsystem = sdl_context.video().unwrap();
    let ttf_context = sdl2::ttf::init().unwrap();

    let window = video_subsystem
        .window("Aecidium Roguelike", 1024, 768)
        .position_centered()
        .build()
        .unwrap();

    let mut events = sdl_context.event_pump().unwrap();
    let mut canvas = window
        .into_canvas()
        .accelerated()
        .present_vsync()
        .build()
        .unwrap();

    unsafe {
        SDL_SetHint(
            SDL_HINT_RENDER_SCALE_QUALITY.as_ptr() as *const i8,
            "0".as_ptr() as *const i8,
        );
    }

    canvas.set_logical_size(1024, 768);

    let texture_creator = canvas.texture_creator();

    let mut texture_manager = TextureManager::new(&texture_creator, String::from("data/sprite/"));

    let font = ttf_context
        .load_font(Path::new("data/font/FreeMono.ttf"), 20)
        .unwrap();
    //font.set_style(sdl2::ttf::STYLE_BOLD);

    let mut font_manager = FontManager::new(&texture_creator, font);

    let mut graphics = Graphics {
        canvas: canvas,
        res_w: 1024,
        res_h: 768,
        texture_manager: texture_manager,
        font_manager: font_manager,
    };

    let mut world = World::new(graphics);

    world.ecs.register_component::<Position>();
    world.ecs.register_component::<Health>();
    world.ecs.register_component::<Name>();
    world.ecs.register_component::<Renderable>();
    world.ecs.register_component::<KeyboardControlled>();
    world.ecs.register_component::<Clusterbomb>();
    world.ecs.register_component::<Item>();
    world.ecs.register_component::<NPC>();
    world.ecs.register_component::<Effects>();
    world.ecs.register_component::<Attacking>();
    world.ecs.register_component::<Entangled>();
    world.ecs.register_component::<EntangleResistant>();
    world.ecs.register_component::<Web>();
    world.ecs.register_component::<Fire>();
    world.ecs.register_component::<FireResistant>();
    world.ecs.register_component::<BaseAttack>();
    world.ecs.register_component::<Armed>();
    world.ecs.register_component::<Shielded>();
    world.ecs.register_component::<Armored>();
    world.ecs.register_component::<Inventory>();
    world.ecs.register_component::<Vision>();

    world.ecs.maintain();

    let mut statemanager = StateManager::new();
    statemanager.push(Box::new(states::mainmenu::MainMenuState::new()));

    let mut gamestate_changed = true;

    'mainloop: loop {
        if !world.running {
            break 'mainloop;
        }

        // Event handling
        for event in events.poll_iter() {
            match event {
                Event::Quit { .. } => break 'mainloop,
                Event::KeyDown {
                    keycode: Option::Some(key),
                    ..
                } => {
                    world.key = Some(key);
                }
                _ => gamestate_changed = true,
            }
        }

        if world.key.is_some() || world.skip_turn {
            gamestate_changed = true;
            world.skip_turn = false;
            statemanager.update(&mut world);
            world.key = None;
        }

        // Rendering

        if gamestate_changed {
            world.graphics.canvas.set_draw_color(Color::RGB(0, 0, 0));

            world.graphics.canvas.clear();

            statemanager.draw(&mut world);

            // Reset the change flag and update screen
            gamestate_changed = false;
            world.graphics.canvas.present();
        } else {
            thread::sleep(time::Duration::from_millis(16));
        }

        world.ecs.maintain();
    }

    return Ok(String::from("Quit cleanly."));
}
