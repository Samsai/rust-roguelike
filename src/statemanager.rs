use std::collections::LinkedList;
use std::rc::Rc;

use *;

pub trait State {
    fn draw(&mut self, world: &mut World) -> Result<String, String>;
    fn update(&mut self, world: &mut World) -> Transition;
}

pub enum Transition {
    Switch(Box<dyn State>),
    Push(Box<dyn State>),
    Continue,
    Destroy,
}

pub struct StateManager {
    stack: LinkedList<Box<dyn State>>,
}

impl StateManager {
    pub fn new() -> StateManager {
        return StateManager {
            stack: LinkedList::new(),
        };
    }

    pub fn push(&mut self, new_state: Box<dyn State>) {
        self.stack.push_back(new_state);
    }

    pub fn draw(&mut self, world: &mut World) -> Result<String, String> {
        for state in self.stack.iter_mut() {
            state.draw(world)?;
        }

        return Ok(String::from("ok"));
    }

    pub fn update(&mut self, world: &mut World) {
        let mut state_opt = self.stack.pop_back();

        match state_opt {
            Some(mut state) => {
                let transition = state.update(world);

                match transition {
                    Transition::Switch(new_state) => self.stack.push_back(new_state),
                    Transition::Push(new_state) => {
                        self.stack.push_back(state);
                        self.stack.push_back(new_state);
                    }
                    Transition::Continue => self.stack.push_back(state),
                    Transition::Destroy => (),
                }
            }
            None => (),
        }
    }
}
