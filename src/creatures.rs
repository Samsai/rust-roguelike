use rand::*;

use std::cell::Cell;

use std::rc::Rc;

use system::dungeon::dijkstra_pathfinder;
use system::dungeon::get_npc_at;

use component::*;
use entity::*;
use helper::*;
use *;

#[derive(Clone)]
pub enum Attack<'a> {
    Physical(&'a str),
    Poison(&'a str),
    Fire(&'a str),
}

pub enum Action {
    MoveUp,
    MoveDown,
    MoveLeft,
    MoveRight,
    Wait,
    Attack,
}

pub fn create_player(world: &mut World) {
    let mut player = &world.player;

    let (x, y) = world.level.rooms[0].center();

    let mut ecs = &mut world.ecs;

    let mut inventory: Vec<Item> = Vec::new();

    inventory.push(Item::Consumable(Rc::new(HealingPotion::new())));
    inventory.push(Item::Consumable(Rc::new(Torch::new())));

    ecs.set_component(
        player,
        Position {
            x: x as i32,
            y: y as i32,
        },
    );
    ecs.set_component(player, Health { hp: 20, max_hp: 20 });
    ecs.set_component(
        player,
        Name {
            name: String::from("Player"),
        },
    );
    ecs.set_component(
        player,
        Renderable {
            sprite: String::from("player.png"),
            item: false,
        },
    );
    ecs.set_component(
        player,
        BaseAttack {
            attack: Attack::Physical("1d3"),
        },
    );
    ecs.set_component(
        player,
        Inventory {
            items: inventory,
            cap: 20,
        },
    );
    ecs.set_component(
        player,
        Vision {
            range: 5,
            max_range: 5,
        },
    );
    ecs.set_component(player, KeyboardControlled {});
}

pub fn create_skeleton(world: &mut World) -> Entity {
    let mut skeleton = world.ecs.create_entity();

    let mut rng = thread_rng();

    let (rx, ry) = world.level.rooms[rng.gen_range(1, world.level.rooms.len())].any_point();

    let ecs = &world.ecs;

    ecs.set_component(
        &skeleton,
        Position {
            x: rx as i32,
            y: ry as i32,
        },
    );
    ecs.set_component(&skeleton, Health { hp: 10, max_hp: 10 });
    ecs.set_component(
        &skeleton,
        Name {
            name: String::from("Skeleton"),
        },
    );
    ecs.set_component(
        &skeleton,
        Renderable {
            sprite: String::from("skeleton.png"),
            item: false,
        },
    );
    ecs.set_component(
        &skeleton,
        BaseAttack {
            attack: Attack::Physical("1d3"),
        },
    );
    ecs.set_component(
        &skeleton,
        Armed {
            weapon: Rc::new(Sword::new()),
        },
    );
    ecs.set_component(
        &skeleton,
        Shielded {
            shield: Rc::new(Roundshield::new()),
        },
    );
    ecs.set_component(
        &skeleton,
        NPC {
            brain: Rc::new(SkeletonBrain::new()),
        },
    );
    ecs.set_component(
        &skeleton,
        Vision {
            range: 5,
            max_range: 5,
        },
    );

    return skeleton;
}

pub fn create_slime(world: &mut World) -> Entity {
    let mut slime = world.ecs.create_entity();

    let mut rng = thread_rng();

    let (rx, ry) = world.level.rooms[rng.gen_range(1, world.level.rooms.len())].any_point();

    let ecs = &world.ecs;

    ecs.set_component(
        &slime,
        Position {
            x: rx as i32,
            y: ry as i32,
        },
    );
    ecs.set_component(&slime, Health { hp: 5, max_hp: 5 });
    ecs.set_component(
        &slime,
        Name {
            name: String::from("Slime"),
        },
    );
    ecs.set_component(
        &slime,
        Renderable {
            sprite: String::from("slime.png"),
            item: false,
        },
    );
    ecs.set_component(
        &slime,
        BaseAttack {
            attack: Attack::Poison("1d2"),
        },
    );
    ecs.set_component(
        &slime,
        NPC {
            brain: Rc::new(SlimeBrain::new()),
        },
    );
    ecs.set_component(
        &slime,
        Vision {
            range: 2,
            max_range: 2,
        },
    );

    return slime;
}

pub fn create_bat(world: &mut World) -> Entity {
    let mut bat = world.ecs.create_entity();

    let mut rng = &mut world.rng;

    let (rx, ry) = world.level.rooms[rng.gen_range(1, world.level.rooms.len())].any_point();

    let ecs = &world.ecs;

    ecs.set_component(
        &bat,
        Position {
            x: rx as i32,
            y: ry as i32,
        },
    );
    ecs.set_component(&bat, Health { hp: 3, max_hp: 3 });
    ecs.set_component(
        &bat,
        Name {
            name: String::from("Bat"),
        },
    );
    ecs.set_component(
        &bat,
        Renderable {
            sprite: String::from("bat.png"),
            item: false,
        },
    );
    ecs.set_component(
        &bat,
        BaseAttack {
            attack: Attack::Physical("1d2"),
        },
    );
    ecs.set_component(
        &bat,
        NPC {
            brain: Rc::new(BatBrain::new()),
        },
    );
    ecs.set_component(
        &bat,
        Vision {
            range: 10,
            max_range: 10,
        },
    );

    return bat;
}

pub fn create_thief(world: &mut World) -> Entity {
    let mut thief = world.ecs.create_entity();

    let mut rng = &mut world.rng;

    let (rx, ry) = world.level.rooms[rng.gen_range(1, world.level.rooms.len())].any_point();

    let ecs = &world.ecs;

    ecs.set_component(
        &thief,
        Position {
            x: rx as i32,
            y: ry as i32,
        },
    );
    ecs.set_component(&thief, Health { hp: 8, max_hp: 8 });
    ecs.set_component(
        &thief,
        Name {
            name: String::from("Thief"),
        },
    );
    ecs.set_component(
        &thief,
        Renderable {
            sprite: String::from("thief.png"),
            item: false,
        },
    );
    ecs.set_component(
        &thief,
        BaseAttack {
            attack: Attack::Physical("1d5"),
        },
    );
    ecs.set_component(
        &thief,
        Armed {
            weapon: Rc::new(Dagger::new()),
        },
    );
    ecs.set_component(
        &thief,
        Inventory {
            items: Vec::new(),
            cap: 5,
        },
    );
    ecs.set_component(
        &thief,
        NPC {
            brain: Rc::new(ThiefBrain::new()),
        },
    );
    ecs.set_component(
        &thief,
        Vision {
            range: 6,
            max_range: 6,
        },
    );

    return thief;
}

pub fn create_beholder(world: &mut World) -> Entity {
    let mut beholder = world.ecs.create_entity();

    let mut rng = &mut world.rng;

    let (rx, ry) = world.level.rooms[rng.gen_range(1, world.level.rooms.len())].any_point();

    let ecs = &world.ecs;

    ecs.set_component(
        &beholder,
        Position {
            x: rx as i32,
            y: ry as i32,
        },
    );
    ecs.set_component(&beholder, Health { hp: 15, max_hp: 15 });
    ecs.set_component(
        &beholder,
        Name {
            name: String::from("Beholder"),
        },
    );
    ecs.set_component(
        &beholder,
        Renderable {
            sprite: String::from("beholder.png"),
            item: false,
        },
    );
    ecs.set_component(
        &beholder,
        BaseAttack {
            attack: Attack::Physical("2d5"),
        },
    );
    ecs.set_component(
        &beholder,
        NPC {
            brain: Rc::new(BeholderBrain::new()),
        },
    );
    ecs.set_component(
        &beholder,
        Vision {
            range: 10,
            max_range: 10,
        },
    );

    return beholder;
}

pub fn create_ghost(world: &mut World) -> Entity {
    let mut ghost = world.ecs.create_entity();

    let mut rng = &mut world.rng;

    let (rx, ry) = world.level.rooms[rng.gen_range(1, world.level.rooms.len())].any_point();

    let ecs = &world.ecs;

    ecs.set_component(
        &ghost,
        Position {
            x: rx as i32,
            y: ry as i32,
        },
    );
    ecs.set_component(&ghost, Health { hp: 10, max_hp: 10 });
    ecs.set_component(
        &ghost,
        Name {
            name: String::from("Ghost"),
        },
    );
    ecs.set_component(
        &ghost,
        Renderable {
            sprite: String::from("ghost.png"),
            item: false,
        },
    );
    ecs.set_component(
        &ghost,
        BaseAttack {
            attack: Attack::Physical("1d4"),
        },
    );
    ecs.set_component(
        &ghost,
        NPC {
            brain: Rc::new(GhostBrain::new()),
        },
    );
    ecs.set_component(
        &ghost,
        Vision {
            range: 10,
            max_range: 10,
        },
    );

    return ghost;
}

pub fn create_spider(world: &mut World) -> Entity {
    let mut spider = world.ecs.create_entity();

    let mut rng = &mut world.rng;

    let (rx, ry) = world.level.rooms[rng.gen_range(1, world.level.rooms.len())].any_point();

    let ecs = &world.ecs;

    ecs.set_component(
        &spider,
        Position {
            x: rx as i32,
            y: ry as i32,
        },
    );
    ecs.set_component(&spider, Health { hp: 8, max_hp: 8 });
    ecs.set_component(
        &spider,
        Name {
            name: String::from("Spider"),
        },
    );
    ecs.set_component(
        &spider,
        Renderable {
            sprite: String::from("spider.png"),
            item: false,
        },
    );
    ecs.set_component(
        &spider,
        BaseAttack {
            attack: Attack::Poison("1d5"),
        },
    );
    ecs.set_component(
        &spider,
        EntangleResistant {
            turns: 1,
            permanent: true,
        },
    );
    ecs.set_component(
        &spider,
        NPC {
            brain: Rc::new(SpiderBrain::new()),
        },
    );
    ecs.set_component(
        &spider,
        Vision {
            range: 5,
            max_range: 5,
        },
    );

    return spider;
}

pub fn create_dragon(world: &mut World) -> Entity {
    let mut dragon = world.ecs.create_entity();

    let mut rng = &mut world.rng;

    let (rx, ry) = world.level.rooms[rng.gen_range(1, world.level.rooms.len())].any_point();

    let ecs = &world.ecs;

    ecs.set_component(
        &dragon,
        Position {
            x: rx as i32,
            y: ry as i32,
        },
    );
    ecs.set_component(&dragon, Health { hp: 20, max_hp: 20 });
    ecs.set_component(
        &dragon,
        Name {
            name: String::from("Dragon"),
        },
    );
    ecs.set_component(
        &dragon,
        Renderable {
            sprite: String::from("dragon.png"),
            item: false,
        },
    );
    ecs.set_component(
        &dragon,
        BaseAttack {
            attack: Attack::Physical("2d6"),
        },
    );
    ecs.set_component(
        &dragon,
        FireResistant {
            turns: 1,
            permanent: true,
        },
    );
    ecs.set_component(
        &dragon,
        NPC {
            brain: Rc::new(DragonBrain::new()),
        },
    );
    ecs.set_component(
        &dragon,
        Vision {
            range: 6,
            max_range: 6,
        },
    );

    return dragon;
}

pub trait Brain {
    fn next_action(&self, world: &World, entity: &Entity) -> Action;
}

pub struct SkeletonBrain {
    last_player_pos: Cell<Option<(i32, i32)>>,
}

impl SkeletonBrain {
    fn new() -> SkeletonBrain {
        return SkeletonBrain {
            last_player_pos: Cell::new(None),
        };
    }
}

impl Brain for SkeletonBrain {
    fn next_action(&self, world: &World, entity: &Entity) -> Action {
        let position_components = world
            .ecs
            .get_components::<Position>()
            .unwrap()
            .read()
            .unwrap();
        let vision_components = world
            .ecs
            .get_components::<Vision>()
            .unwrap()
            .read()
            .unwrap();

        let pos = position_components.get(entity).unwrap();
        let player_pos = position_components.get(&world.player).unwrap();
        let vision = vision_components.get(entity).unwrap();

        if are_adjacent(pos, player_pos) {
            return Action::Attack;
        }

        let los = world.level.raycast_vision(pos.x, pos.y, vision.range);

        if los.contains(&(player_pos.x, player_pos.y)) {
            self.last_player_pos
                .replace(Some((player_pos.x, player_pos.y)));

            let (dir, len) = dijkstra_pathfinder(world, &pos, &player_pos);

            let dir = dir[0];

            if thread_rng().gen::<u8>() < 220 {
                return match dir {
                    Direction::Up => Action::MoveUp,
                    Direction::Down => Action::MoveDown,
                    Direction::Left => Action::MoveLeft,
                    Direction::Right => Action::MoveRight,
                    _ => Action::Wait,
                };
            }
        } else if let Some((x, y)) = self.last_player_pos.get() {
            if pos.x == x && pos.y == y {
                self.last_player_pos.replace(None);
            } else {
                let (dir, len) = dijkstra_pathfinder(world, &pos, &Position { x, y });

                let dir = dir[0];

                return match dir {
                    Direction::Up => Action::MoveUp,
                    Direction::Down => Action::MoveDown,
                    Direction::Left => Action::MoveLeft,
                    Direction::Right => Action::MoveRight,
                    _ => Action::Wait,
                };
            }
        }

        let action = thread_rng().gen_range(0, 8);

        return match action {
            0 => Action::MoveUp,
            1 => Action::MoveDown,
            2 => Action::MoveLeft,
            3 => Action::MoveRight,
            _ => Action::Wait,
        };
    }
}

pub struct SlimeBrain {
    divided: Cell<bool>,
}

impl SlimeBrain {
    fn new() -> SlimeBrain {
        return SlimeBrain {
            divided: Cell::new(false),
        };
    }
}

impl Brain for SlimeBrain {
    fn next_action(&self, world: &World, entity: &Entity) -> Action {
        let position_components = world
            .ecs
            .get_components::<Position>()
            .unwrap()
            .read()
            .unwrap();
        let health_components = world
            .ecs
            .get_components::<Health>()
            .unwrap()
            .read()
            .unwrap();
        let vision_components = world
            .ecs
            .get_components::<Vision>()
            .unwrap()
            .read()
            .unwrap();

        let player_pos = position_components.get(&world.player).unwrap();
        let pos = position_components.get(entity).unwrap();
        let health = health_components.get(entity).unwrap();
        let vision = vision_components.get(entity).unwrap();

        // If wounded, split off into multiple slimes
        if health.hp < health.max_hp && !self.divided.get() {
            let mut slime = world.ecs.create_entity();

            let ecs = &world.ecs;

            ecs.set_component_delayed(&slime, Position { x: pos.x, y: pos.y });
            ecs.set_component_delayed(
                &slime,
                Health {
                    hp: health.hp,
                    max_hp: health.hp,
                },
            );
            ecs.set_component_delayed(
                &slime,
                Name {
                    name: String::from("Slime"),
                },
            );
            ecs.set_component_delayed(
                &slime,
                Renderable {
                    sprite: String::from("slime.png"),
                    item: false,
                },
            );
            ecs.set_component_delayed(
                &slime,
                BaseAttack {
                    attack: Attack::Poison("1d2"),
                },
            );
            ecs.set_component_delayed(
                &slime,
                NPC {
                    brain: Rc::new(SlimeBrain::new()),
                },
            );
            ecs.set_component_delayed(
                &slime,
                Vision {
                    range: 2,
                    max_range: 2,
                },
            );

            self.divided.set(true);

            return Action::Wait;
        }

        if are_adjacent(pos, player_pos) {
            return Action::Attack;
        }

        let los = world.level.raycast_vision(pos.x, pos.y, vision.range);

        if los.contains(&(player_pos.x, player_pos.y)) {
            let (dir, len) = dijkstra_pathfinder(world, pos, player_pos);

            let dir = dir[0];

            if thread_rng().gen::<u8>() < 220 {
                return match dir {
                    Direction::Up => Action::MoveUp,
                    Direction::Down => Action::MoveDown,
                    Direction::Left => Action::MoveLeft,
                    Direction::Right => Action::MoveRight,
                    _ => Action::Wait,
                };
            }
        }

        let action = thread_rng().gen_range(0, 8);

        return match action {
            0 => Action::MoveUp,
            1 => Action::MoveDown,
            2 => Action::MoveLeft,
            3 => Action::MoveRight,
            _ => Action::Wait,
        };
    }
}

pub struct BatBrain {
    last_player_pos: Cell<Option<(i32, i32)>>,
}

impl BatBrain {
    fn new() -> BatBrain {
        return BatBrain {
            last_player_pos: Cell::new(None),
        };
    }
}

impl Brain for BatBrain {
    fn next_action(&self, world: &World, entity: &Entity) -> Action {
        let position_components = world
            .ecs
            .get_components::<Position>()
            .unwrap()
            .read()
            .unwrap();
        let health_components = world
            .ecs
            .get_components::<Health>()
            .unwrap()
            .read()
            .unwrap();
        let vision_components = world
            .ecs
            .get_components::<Vision>()
            .unwrap()
            .read()
            .unwrap();

        let player_pos = position_components.get(&world.player).unwrap();
        let pos = position_components.get(entity).unwrap();
        let health = health_components.get(entity).unwrap();
        let vision = vision_components.get(entity).unwrap();

        if are_adjacent(pos, player_pos) {
            return Action::Attack;
        }

        let los = world.level.raycast_vision(pos.x, pos.y, vision.range);

        if los.contains(&(player_pos.x, player_pos.y)) {
            self.last_player_pos
                .replace(Some((player_pos.x, player_pos.y)));

            let (dir, len) = dijkstra_pathfinder(world, &pos, &player_pos);

            let dir = dir[0];

            return match dir {
                Direction::Up => Action::MoveUp,
                Direction::Down => Action::MoveDown,
                Direction::Left => Action::MoveLeft,
                Direction::Right => Action::MoveRight,
                _ => Action::Wait,
            };
        } else if let Some((x, y)) = self.last_player_pos.get() {
            if pos.x == x && pos.y == y {
                self.last_player_pos.replace(None);
            } else {
                let (dir, len) = dijkstra_pathfinder(world, &pos, &Position { x, y });

                let dir = dir[0];

                return match dir {
                    Direction::Up => Action::MoveUp,
                    Direction::Down => Action::MoveDown,
                    Direction::Left => Action::MoveLeft,
                    Direction::Right => Action::MoveRight,
                    _ => Action::Wait,
                };
            }
        }

        let action = thread_rng().gen_range(0, 8);

        return match action {
            0 => Action::MoveUp,
            1 => Action::MoveDown,
            2 => Action::MoveLeft,
            3 => Action::MoveRight,
            _ => Action::Wait,
        };
    }
}

pub struct ThiefBrain {
    has_stolen: Cell<bool>,
}

impl ThiefBrain {
    fn new() -> ThiefBrain {
        return ThiefBrain {
            has_stolen: Cell::new(false),
        };
    }
}

impl Brain for ThiefBrain {
    fn next_action(&self, world: &World, entity: &Entity) -> Action {
        let position_components = world
            .ecs
            .get_components::<Position>()
            .unwrap()
            .read()
            .unwrap();
        let vision_components = world
            .ecs
            .get_components::<Vision>()
            .unwrap()
            .read()
            .unwrap();
        let mut inventory_components = world
            .ecs
            .get_components::<Inventory>()
            .unwrap()
            .write()
            .unwrap();

        let player_pos = position_components.get(&world.player).unwrap();
        let pos = position_components.get(entity).unwrap();
        let vision = vision_components.get(entity).unwrap();

        let los = world.level.raycast_vision(pos.x, pos.y, vision.range);

        if self.has_stolen.get() {
            if are_adjacent(pos, player_pos) {
                return Action::Attack;
            }

            let (dir, len) = dijkstra_pathfinder(world, &pos, &player_pos);

            let dir = dir[0];

            return match dir {
                Direction::Up => Action::MoveDown,
                Direction::Down => Action::MoveUp,
                Direction::Left => Action::MoveRight,
                Direction::Right => Action::MoveLeft,
                _ => Action::Wait,
            };
        } else if are_adjacent(pos, player_pos) {
            if thread_rng().gen_range(0, 10) < 3 {
                let item = {
                    let mut player_inventory = inventory_components.get_mut(&world.player).unwrap();

                    if player_inventory.items.is_empty() {
                        None
                    } else {
                        Some(
                            player_inventory.items.swap_remove(
                                thread_rng().gen_range(0, player_inventory.items.len()),
                            ),
                        )
                    }
                };

                if let Some(item) = item {
                    let mut log = world.log.borrow_mut();

                    log.push(format!("Thief steals a {}", item.name()));

                    let mut thief_inventory = inventory_components.get_mut(entity).unwrap();

                    thief_inventory.items.push(item);

                    self.has_stolen.set(true);
                }

                let (dir, len) = dijkstra_pathfinder(world, &pos, &player_pos);

                let dir = dir[0];

                return match dir {
                    Direction::Up => Action::MoveDown,
                    Direction::Down => Action::MoveUp,
                    Direction::Left => Action::MoveRight,
                    Direction::Right => Action::MoveLeft,
                    _ => Action::Wait,
                };
            } else {
                return Action::Attack;
            }
        } else if los.contains(&(player_pos.x, player_pos.y)) {
            let (dir, len) = dijkstra_pathfinder(world, &pos, &player_pos);

            let dir = dir[0];

            if thread_rng().gen::<u8>() < 220 {
                return match dir {
                    Direction::Up => Action::MoveUp,
                    Direction::Down => Action::MoveDown,
                    Direction::Left => Action::MoveLeft,
                    Direction::Right => Action::MoveRight,
                    _ => Action::Wait,
                };
            }
        }

        let action = thread_rng().gen_range(0, 8);

        return match action {
            0 => Action::MoveUp,
            1 => Action::MoveDown,
            2 => Action::MoveLeft,
            3 => Action::MoveRight,
            _ => Action::Wait,
        };
    }
}

pub struct GhostBrain {
    last_player_pos: Cell<Option<(i32, i32)>>,
    teleport_timer: Cell<i32>,
}

impl GhostBrain {
    fn new() -> GhostBrain {
        return GhostBrain {
            last_player_pos: Cell::new(None),
            teleport_timer: Cell::new(0),
        };
    }
}

impl Brain for GhostBrain {
    fn next_action(&self, world: &World, entity: &Entity) -> Action {
        self.teleport_timer.replace(self.teleport_timer.get() + 1);

        let mut rng = thread_rng();

        if self.teleport_timer.get() > 15 {
            if rng.gen_range(1, 5) == 1 {
                let mut log = world.log.borrow_mut();

                self.teleport_timer.replace(0);

                loop {
                    let (x, y) =
                        world.level.rooms[rng.gen_range(0, world.level.rooms.len())].any_point();

                    if let None = get_npc_at(
                        world,
                        &Position {
                            x: x as i32,
                            y: y as i32,
                        },
                    ) {
                        let mut position_components = world
                            .ecs
                            .get_components::<Position>()
                            .unwrap()
                            .write()
                            .unwrap();
                        let mut pos = position_components.get_mut(entity).unwrap();

                        pos.x = x as i32;
                        pos.y = y as i32;

                        break;
                    }
                }
            }
        }

        let position_components = world
            .ecs
            .get_components::<Position>()
            .unwrap()
            .read()
            .unwrap();
        let vision_components = world
            .ecs
            .get_components::<Vision>()
            .unwrap()
            .read()
            .unwrap();

        let player_pos = position_components.get(&world.player).unwrap();
        let pos = position_components.get(entity).unwrap();
        let vision = vision_components.get(entity).unwrap();

        if are_adjacent(pos, player_pos) {
            return Action::Attack;
        }
        let los = world.level.raycast_vision(pos.x, pos.y, vision.range);

        if los.contains(&(player_pos.x, player_pos.y)) {
            self.last_player_pos
                .replace(Some((player_pos.x, player_pos.y)));

            let (dir, len) = dijkstra_pathfinder(world, &pos, &player_pos);

            let dir = dir[0];

            return match dir {
                Direction::Up => Action::MoveUp,
                Direction::Down => Action::MoveDown,
                Direction::Left => Action::MoveLeft,
                Direction::Right => Action::MoveRight,
                _ => Action::Wait,
            };
        } else if let Some((x, y)) = self.last_player_pos.get() {
            if pos.x == x && pos.y == y {
                self.last_player_pos.replace(None);
            } else {
                let (dir, len) = dijkstra_pathfinder(world, &pos, &Position { x, y });

                let dir = dir[0];

                return match dir {
                    Direction::Up => Action::MoveUp,
                    Direction::Down => Action::MoveDown,
                    Direction::Left => Action::MoveLeft,
                    Direction::Right => Action::MoveRight,
                    _ => Action::Wait,
                };
            }
        }

        let action = thread_rng().gen_range(0, 8);

        return match action {
            0 => Action::MoveUp,
            1 => Action::MoveDown,
            2 => Action::MoveLeft,
            3 => Action::MoveRight,
            _ => Action::Wait,
        };
    }
}

pub struct BeholderBrain {
    last_player_pos: Cell<Option<(i32, i32)>>,
    fire_timer: Cell<i32>,
}

impl BeholderBrain {
    fn new() -> BeholderBrain {
        return BeholderBrain {
            last_player_pos: Cell::new(None),
            fire_timer: Cell::new(0),
        };
    }
}

impl Brain for BeholderBrain {
    fn next_action(&self, world: &World, entity: &Entity) -> Action {
        let position_components = world
            .ecs
            .get_components::<Position>()
            .unwrap()
            .read()
            .unwrap();
        let health_components = world
            .ecs
            .get_components::<Health>()
            .unwrap()
            .read()
            .unwrap();
        let vision_components = world
            .ecs
            .get_components::<Vision>()
            .unwrap()
            .read()
            .unwrap();

        let player_pos = position_components.get(&world.player).unwrap();
        let pos = position_components.get(entity).unwrap();
        let health = health_components.get(entity).unwrap();
        let vision = vision_components.get(entity).unwrap();

        let mut rng = thread_rng();

        self.fire_timer.set(self.fire_timer.get() + 1);

        if are_adjacent(pos, player_pos) {
            return Action::Attack;
        }

        let los = world.level.raycast_vision(pos.x, pos.y, vision.range);

        if los.contains(&(player_pos.x, player_pos.y)) {
            if self.fire_timer.get() > 5 {
                if rng.gen_range(1, 5) == 1 {
                    let los = world.level.bresenham(
                        (pos.x, pos.y),
                        (player_pos.x, player_pos.y),
                        vision.range,
                    );

                    let mut blocked = false;

                    for (x, y) in los.iter().skip(1) {
                        if let Some(npc) = get_npc_at(world, &Position { x: *x, y: *y }) {
                            blocked = true;
                            break;
                        }
                    }

                    if !blocked {
                        let (x, y) = world
                            .level
                            .bresenham((pos.x, pos.y), (player_pos.x, player_pos.y), vision.range)
                            .iter()
                            .skip(1)
                            .next()
                            .unwrap()
                            .clone();

                        self.fire_timer.replace(0);

                        let fireball = world.ecs.create_entity();

                        world
                            .ecs
                            .set_component_delayed(&fireball, Position { x, y });

                        world.ecs.set_component_delayed(
                            &fireball,
                            Name {
                                name: String::from("Fireball"),
                            },
                        );
                        world
                            .ecs
                            .set_component_delayed(&fireball, Health { hp: 99, max_hp: 99 });
                        world.ecs.set_component_delayed(
                            &fireball,
                            Renderable {
                                sprite: String::from("fireball.png"),
                                item: false,
                            },
                        );
                        world.ecs.set_component_delayed(
                            &fireball,
                            Vision {
                                range: 10,
                                max_range: 10,
                            },
                        );
                        world.ecs.set_component_delayed(
                            &fireball,
                            NPC {
                                brain: Rc::new(ProjectileBrain::new()),
                            },
                        );
                        world.ecs.set_component_delayed(
                            &fireball,
                            BaseAttack {
                                attack: Attack::Fire("1d5"),
                            },
                        );
                    }

                    let mut log = world.log.borrow_mut();

                    log.push(String::from("Beholder fires a fireball!"));

                    return Action::Wait;
                }
            }

            self.last_player_pos
                .replace(Some((player_pos.x, player_pos.y)));

            let (dir, len) = dijkstra_pathfinder(world, &pos, &player_pos);

            let dir = dir[0];

            return match dir {
                Direction::Up => Action::MoveUp,
                Direction::Down => Action::MoveDown,
                Direction::Left => Action::MoveLeft,
                Direction::Right => Action::MoveRight,
                _ => Action::Wait,
            };
        } else if let Some((x, y)) = self.last_player_pos.get() {
            if pos.x == x && pos.y == y {
                self.last_player_pos.replace(None);
            } else {
                let (dir, len) = dijkstra_pathfinder(world, &pos, &Position { x, y });

                let dir = dir[0];

                return match dir {
                    Direction::Up => Action::MoveUp,
                    Direction::Down => Action::MoveDown,
                    Direction::Left => Action::MoveLeft,
                    Direction::Right => Action::MoveRight,
                    _ => Action::Wait,
                };
            }
        }

        let action = thread_rng().gen_range(0, 8);

        return match action {
            0 => Action::MoveUp,
            1 => Action::MoveDown,
            2 => Action::MoveLeft,
            3 => Action::MoveRight,
            _ => Action::Wait,
        };
    }
}

pub struct DragonBrain {
    last_player_pos: Cell<Option<(i32, i32)>>,
    fire_timer: Cell<i32>,
}

impl DragonBrain {
    fn new() -> DragonBrain {
        return DragonBrain {
            last_player_pos: Cell::new(None),
            fire_timer: Cell::new(0),
        };
    }
}

impl Brain for DragonBrain {
    fn next_action(&self, world: &World, entity: &Entity) -> Action {
        let position_components = world
            .ecs
            .get_components::<Position>()
            .unwrap()
            .read()
            .unwrap();
        let health_components = world
            .ecs
            .get_components::<Health>()
            .unwrap()
            .read()
            .unwrap();
        let vision_components = world
            .ecs
            .get_components::<Vision>()
            .unwrap()
            .read()
            .unwrap();

        let player_pos = position_components.get(&world.player).unwrap();
        let pos = position_components.get(entity).unwrap();
        let health = health_components.get(entity).unwrap();
        let vision = vision_components.get(entity).unwrap();

        let mut rng = thread_rng();

        self.fire_timer.set(self.fire_timer.get() + 1);

        if are_adjacent(pos, player_pos) {
            return Action::Attack;
        }

        let los = world.level.raycast_vision(pos.x, pos.y, vision.range);

        if los.contains(&(player_pos.x, player_pos.y)) {
            if self.fire_timer.get() > 5 {
                if rng.gen_range(1, 5) == 1 {
                    let los = world.level.bresenham(
                        (pos.x, pos.y),
                        (player_pos.x, player_pos.y),
                        vision.range,
                    );

                    let mut blocked = false;

                    for (x, y) in los.iter().skip(1) {
                        if let Some(npc) = get_npc_at(world, &Position { x: *x, y: *y }) {
                            blocked = true;
                            break;
                        }
                    }

                    if !blocked {
                        self.fire_timer.replace(0);

                        for (x, y) in los.iter().skip(1) {
                            let fire = world.ecs.create_entity();

                            world
                                .ecs
                                .set_component_delayed(&fire, Position { x: *x, y: *y });
                            world.ecs.set_component_delayed(&fire, Fire { lifetime: 3 });
                            world.ecs.set_component_delayed(
                                &fire,
                                Renderable {
                                    sprite: String::from("fire.png"),
                                    item: false,
                                },
                            );
                        }
                    }

                    let mut log = world.log.borrow_mut();

                    log.push(String::from("Beholder fires a fireball!"));

                    return Action::Wait;
                }
            }

            self.last_player_pos
                .replace(Some((player_pos.x, player_pos.y)));

            let (dir, len) = dijkstra_pathfinder(world, &pos, &player_pos);

            let dir = dir[0];

            return match dir {
                Direction::Up => Action::MoveUp,
                Direction::Down => Action::MoveDown,
                Direction::Left => Action::MoveLeft,
                Direction::Right => Action::MoveRight,
                _ => Action::Wait,
            };
        } else if let Some((x, y)) = self.last_player_pos.get() {
            if pos.x == x && pos.y == y {
                self.last_player_pos.replace(None);
            } else {
                let (dir, len) = dijkstra_pathfinder(world, &pos, &Position { x, y });

                let dir = dir[0];

                return match dir {
                    Direction::Up => Action::MoveUp,
                    Direction::Down => Action::MoveDown,
                    Direction::Left => Action::MoveLeft,
                    Direction::Right => Action::MoveRight,
                    _ => Action::Wait,
                };
            }
        }

        let action = thread_rng().gen_range(0, 8);

        return match action {
            0 => Action::MoveUp,
            1 => Action::MoveDown,
            2 => Action::MoveLeft,
            3 => Action::MoveRight,
            _ => Action::Wait,
        };
    }
}
pub struct SpiderBrain {
    last_player_pos: Cell<Option<(i32, i32)>>,
    web_timer: Cell<i32>,
}

impl SpiderBrain {
    fn new() -> SpiderBrain {
        return SpiderBrain {
            last_player_pos: Cell::new(None),
            web_timer: Cell::new(0),
        };
    }
}

impl Brain for SpiderBrain {
    fn next_action(&self, world: &World, entity: &Entity) -> Action {
        self.web_timer.replace(self.web_timer.get() + 1);

        let mut rng = thread_rng();

        let position_components = world
            .ecs
            .get_components::<Position>()
            .unwrap()
            .read()
            .unwrap();
        let vision_components = world
            .ecs
            .get_components::<Vision>()
            .unwrap()
            .read()
            .unwrap();

        let player_pos = position_components.get(&world.player).unwrap();
        let pos = position_components.get(entity).unwrap();
        let vision = vision_components.get(entity).unwrap();

        if self.web_timer.get() > 5 {
            if rng.gen_range(1, 5) == 1 {
                let mut log = world.log.borrow_mut();

                self.web_timer.replace(0);

                let web = world.ecs.create_entity();

                world.ecs.set_component_delayed(&web, pos.clone());
                world.ecs.set_component_delayed(
                    &web,
                    Renderable {
                        sprite: String::from("web.png"),
                        item: false,
                    },
                );
                world.ecs.set_component_delayed(&web, Web {});
            }
        }

        if are_adjacent(pos, player_pos) {
            return Action::Attack;
        }
        let los = world.level.raycast_vision(pos.x, pos.y, vision.range);

        if los.contains(&(player_pos.x, player_pos.y)) {
            self.last_player_pos
                .replace(Some((player_pos.x, player_pos.y)));

            let (dir, len) = dijkstra_pathfinder(world, &pos, &player_pos);

            let dir = dir[0];

            return match dir {
                Direction::Up => Action::MoveUp,
                Direction::Down => Action::MoveDown,
                Direction::Left => Action::MoveLeft,
                Direction::Right => Action::MoveRight,
                _ => Action::Wait,
            };
        } else if let Some((x, y)) = self.last_player_pos.get() {
            if pos.x == x && pos.y == y {
                self.last_player_pos.replace(None);
            } else {
                let (dir, len) = dijkstra_pathfinder(world, &pos, &Position { x, y });

                let dir = dir[0];

                return match dir {
                    Direction::Up => Action::MoveUp,
                    Direction::Down => Action::MoveDown,
                    Direction::Left => Action::MoveLeft,
                    Direction::Right => Action::MoveRight,
                    _ => Action::Wait,
                };
            }
        }

        let action = thread_rng().gen_range(0, 8);

        return match action {
            0 => Action::MoveUp,
            1 => Action::MoveDown,
            2 => Action::MoveLeft,
            3 => Action::MoveRight,
            _ => Action::Wait,
        };
    }
}

pub struct ProjectileBrain {
    attacked: Cell<bool>,
}

impl ProjectileBrain {
    fn new() -> ProjectileBrain {
        return ProjectileBrain {
            attacked: Cell::new(false),
        };
    }
}

impl Brain for ProjectileBrain {
    fn next_action(&self, world: &World, entity: &Entity) -> Action {
        let position_components = world
            .ecs
            .get_components::<Position>()
            .unwrap()
            .read()
            .unwrap();
        let vision_components = world
            .ecs
            .get_components::<Vision>()
            .unwrap()
            .read()
            .unwrap();

        let player_pos = position_components.get(&world.player).unwrap();
        let pos = position_components.get(entity).unwrap();
        let vision = vision_components.get(entity).unwrap();

        if self.attacked.get() {
            world.ecs.destroy_entity(entity);
        }

        if are_adjacent(pos, player_pos) {
            self.attacked.set(true);
            world.ecs.remove_component_delayed::<Renderable>(entity);
            world
                .ecs
                .set_component_delayed(entity, Position { x: 100, y: 100 });

            return Action::Attack;
        }

        let los = world.level.raycast_vision(pos.x, pos.y, vision.range);

        if los.contains(&(player_pos.x, player_pos.y)) {
            let (dir, len) = dijkstra_pathfinder(world, &pos, &player_pos);

            let dir = dir[0];

            return match dir {
                Direction::Up => Action::MoveUp,
                Direction::Down => Action::MoveDown,
                Direction::Left => Action::MoveLeft,
                Direction::Right => Action::MoveRight,
                _ => Action::Wait,
            };
        } else {
            world.ecs.destroy_entity(entity);
        }

        let action = thread_rng().gen_range(0, 8);

        return match action {
            0 => Action::MoveUp,
            1 => Action::MoveDown,
            2 => Action::MoveLeft,
            3 => Action::MoveRight,
            _ => Action::Wait,
        };
    }
}
