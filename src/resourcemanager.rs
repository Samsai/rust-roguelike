extern crate sdl2;

use sdl2::image::LoadTexture;
use sdl2::pixels::Color;
use sdl2::rect::Rect;
use sdl2::render::Texture;
use sdl2::render::TextureCreator;
use sdl2::surface::SurfaceContext;
use sdl2::ttf::Font;
use sdl2::video::WindowContext;

use std::cell::RefCell;
use std::collections::HashMap;
use std::path::Path;
use std::rc::Rc;

pub struct TextureManager<'a> {
    tc: &'a TextureCreator<WindowContext>,
    root_path: String,
    textures: HashMap<String, Texture<'a>>,
}

impl<'s> TextureManager<'s> {
    pub fn new<'a>(
        texture_creator: &'a TextureCreator<WindowContext>,
        initial_path: String,
    ) -> TextureManager {
        return TextureManager {
            tc: &texture_creator,
            root_path: initial_path,
            textures: HashMap::new(),
        };
    }

    pub fn load(&mut self, resource: &str) -> &Texture {
        let full_resource_path = String::from(
            Path::new(self.root_path.as_str())
                .join(resource)
                .to_str()
                .unwrap(),
        );

        {
            if self.textures.contains_key(&full_resource_path) {
                return self.textures.get(&full_resource_path).unwrap();
            }
        }

        let new_texture = self
            .tc
            .load_texture(Path::new(self.root_path.as_str()).join(resource).as_path())
            .unwrap();
        self.textures
            .insert(full_resource_path.clone(), new_texture);

        return self.textures.get(&full_resource_path).unwrap().clone();
    }

    fn unload_all(&mut self) {
        self.textures.clear();
    }
}

pub struct FontManager<'a, 'b> {
    tc: &'a TextureCreator<WindowContext>,
    font: RefCell<Font<'a, 'b>>,
}

impl<'s, 't> FontManager<'s, 't> {
    pub fn new<'a, 'b>(
        texture_creator: &'a TextureCreator<WindowContext>,
        font: Font<'a, 'b>,
    ) -> FontManager<'a, 'b> {
        return FontManager {
            tc: texture_creator,
            font: RefCell::new(font),
        };
    }

    pub fn render_rgb(&self, text: &str, color: Color) -> Texture {
        let font = self.font.borrow();

        let surface = font.render(text).blended(color).unwrap();
        let texture = self.tc.create_texture_from_surface(&surface).unwrap();

        return texture;
    }

    pub fn render_rgb_wrapped(&self, text: &str, color: Color, len: u32) -> Texture {
        let font = self.font.borrow();

        let surface = font.render(text).blended_wrapped(color, len).unwrap();
        let texture = self.tc.create_texture_from_surface(&surface).unwrap();

        return texture;
    }

    // Render text with given font in normal
    pub fn render(&self, text: &str) -> Texture {
        return self.render_rgb(text, Color::RGB(255, 255, 255));
    }

    // Render text with given font in normal
    pub fn render_wrapped(&self, text: &str, len: u32) -> Texture {
        return self.render_rgb_wrapped(text, Color::RGB(255, 255, 255), len);
    }

    // Set font to bold and render text, returning font back to normal
    pub fn render_bold(&self, text: &str) -> Texture {
        {
            let mut font = self.font.borrow_mut();

            font.set_style(sdl2::ttf::FontStyle::BOLD);
        }

        let texture = self.render(text);

        let mut font = self.font.borrow_mut();
        font.set_style(sdl2::ttf::FontStyle::NORMAL);

        return texture;
    }
}
