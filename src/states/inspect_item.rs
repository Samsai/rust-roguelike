extern crate sdl2;

use sdl2::keyboard::Keycode;

use item::*;
use statemanager::*;
use World;

use system::System;

use system::inspect_item::*;

pub struct InspectItemState {
    render_sys: RenderInspectItemSystem,
}

impl InspectItemState {
    pub fn new() -> InspectItemState {
        return InspectItemState {
            render_sys: RenderInspectItemSystem::new(),
        };
    }
}

impl State for InspectItemState {
    fn draw(&mut self, world: &mut World) -> Result<String, String> {
        self.render_sys.execute(world);

        return Ok(String::from("ok"));
    }

    fn update(&mut self, world: &mut World) -> Transition {
        if let Some(key) = world.key {
            match key {
                Keycode::Escape => return Transition::Destroy,
                _ => (),
            }
        }

        return Transition::Continue;
    }
}
