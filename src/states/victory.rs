use sdl2::keyboard::Keycode;

use World;

use statemanager::*;

use states::mainmenu::*;

use system::victory::*;
use system::*;

pub struct VictoryState {
    render_victory_sys: RenderVictorySystem,
}

impl VictoryState {
    pub fn new() -> VictoryState {
        return VictoryState {
            render_victory_sys: RenderVictorySystem::new(),
        };
    }
}

impl State for VictoryState {
    fn update(&mut self, world: &mut World) -> Transition {
        if let Some(key) = world.key {
            match key {
                Keycode::Escape => return Transition::Switch(Box::new(MainMenuState::new())),
                _ => (),
            }
        }

        return Transition::Continue;
    }

    fn draw(&mut self, world: &mut World) -> Result<String, String> {
        self.render_victory_sys.execute(world);

        return Ok(String::from("OK"));
    }
}
