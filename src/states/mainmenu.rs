use sdl2::keyboard::Keycode;

use statemanager::*;

use states::dungeon::*;

use system::mainmenu::*;
use system::System;

use system::dungeon::{generate_level, init_new_game};

use World;

pub struct MainMenuState {
    render_sys: RenderMainMenuSystem,
    input_sys: MainMenuInputSystem,
}

impl MainMenuState {
    pub fn new() -> MainMenuState {
        return MainMenuState {
            render_sys: RenderMainMenuSystem::new(),
            input_sys: MainMenuInputSystem::new(),
        };
    }
}

impl State for MainMenuState {
    fn draw(&mut self, world: &mut World) -> Result<String, String> {
        self.render_sys.execute(world);

        return Ok(String::from("OK"));
    }

    fn update(&mut self, world: &mut World) -> Transition {
        self.input_sys.execute(world);

        let selection = world
            .resources
            .get::<MainMenuIndex>()
            .unwrap_or(&MainMenuIndex { index: 0 })
            .index;

        let current_key = world.key;
        if let Some(key) = current_key {
            if let Keycode::Return = key {
                match MENUOPTIONS[selection] {
                    "New Game" => {
                        init_new_game(world);
                        generate_level(world);

                        return Transition::Switch(Box::new(DungeonGameplayState::new()));
                    }
                    "Quit" => world.running = false,
                    _ => (),
                }
            }
        }

        return Transition::Continue;
    }
}
