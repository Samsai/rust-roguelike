use sdl2::keyboard::Keycode;

use statemanager::*;

use states::gameover::*;
use states::inventory::*;
use states::victory::*;

use system::dungeon::*;
use system::ClusterbombSystem;
use system::System;

use component::*;

use World;

use item::Effect;

pub struct DungeonGameplayState {
    render_sys: RenderDungeonSystem,
    render_hud_sys: RenderHUDSystem,
    input_sys: PlayerInputSystem,
    apply_effects_sys: ApplyEffectsSystem,
    entangle_sys: EntangleSystem,
    fire_sys: FireSystem,
    attack_res_sys: AttackResolutionSystem,
    grim_reaper_sys: GrimReaperSystem,
    npc_sys: NPCSystem,
}

impl DungeonGameplayState {
    pub fn new() -> DungeonGameplayState {
        DungeonGameplayState {
            render_sys: RenderDungeonSystem::new(),
            render_hud_sys: RenderHUDSystem::new(),
            input_sys: PlayerInputSystem::new(),
            apply_effects_sys: ApplyEffectsSystem::new(),
            entangle_sys: EntangleSystem::new(),
            fire_sys: FireSystem::new(),
            attack_res_sys: AttackResolutionSystem::new(),
            grim_reaper_sys: GrimReaperSystem::new(),
            npc_sys: NPCSystem::new(),
        }
    }
}

impl State for DungeonGameplayState {
    fn draw(&mut self, world: &mut World) -> Result<String, String> {
        self.render_sys.execute(world);
        self.render_hud_sys.execute(world);

        return Ok(String::from("OK"));
    }

    fn update(&mut self, world: &mut World) -> Transition {
        if let Some(key) = world.key {
            if key == Keycode::I {
                return Transition::Push(Box::new(InventoryState::new()));
            }
        }

        if world.victory.get() {
            return Transition::Switch(Box::new(VictoryState::new()));
        }

        let previous_depth = world.depth;

        self.apply_effects_sys.execute(world);
        self.input_sys.execute(world);
        self.entangle_sys.execute(world);
        self.fire_sys.execute(world);
        self.attack_res_sys.execute(world);
        self.grim_reaper_sys.execute(world);
        self.npc_sys.execute(world);
        self.attack_res_sys.execute(world);

        {
            let health_components = world
                .ecs
                .get_components::<Health>()
                .unwrap()
                .read()
                .unwrap();

            let health = health_components.get(&world.player).unwrap();

            if health.hp <= 0 {
                return Transition::Switch(Box::new(GameOverState::new()));
            }
        }

        let current_depth = world.depth;

        if current_depth > previous_depth {
            {
                let mut health_components = world
                    .ecs
                    .get_components::<Health>()
                    .unwrap()
                    .write()
                    .unwrap();
                let mut health = health_components.get_mut(&world.player).unwrap();

                health.hp += 5;

                if health.hp > health.max_hp {
                    health.hp = health.max_hp;
                }
            }
            generate_level(world);
        }

        return Transition::Continue;
    }
}
