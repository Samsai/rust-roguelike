use sdl2::keyboard::Keycode;

use statemanager::*;
use World;

use states::inspect_item::*;

use system::dungeon::*;
use system::inventory::*;
use system::System;

pub struct InventoryState {
    render_sys: RenderDungeonSystem,
    render_inv_sys: RenderInventorySystem,
    inventory_sys: PlayerInventorySystem,
}

impl InventoryState {
    pub fn new() -> InventoryState {
        return InventoryState {
            render_sys: RenderDungeonSystem::new(),
            render_inv_sys: RenderInventorySystem::new(),
            inventory_sys: PlayerInventorySystem::new(),
        };
    }
}

impl State for InventoryState {
    fn draw(&mut self, world: &mut World) -> Result<String, String> {
        self.render_sys.execute(world);
        self.render_inv_sys.execute(world);

        return Ok(String::from("OK"));
    }

    fn update(&mut self, world: &mut World) -> Transition {
        if let Some(key) = world.key {
            if key == Keycode::Escape {
                return Transition::Destroy;
            }
        }

        self.inventory_sys.execute(world);

        if let Some(key) = world.key {
            if key == Keycode::Return || key == Keycode::D {
                return Transition::Destroy;
            } else if key == Keycode::L {
                return Transition::Push(Box::new(InspectItemState::new()));
            }
        }

        return Transition::Continue;
    }
}
