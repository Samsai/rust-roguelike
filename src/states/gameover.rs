use sdl2::keyboard::Keycode;

use World;

use statemanager::*;

use states::mainmenu::*;

use system::gameover::*;
use system::*;

pub struct GameOverState {
    render_game_over_sys: RenderGameOverSystem,
}

impl GameOverState {
    pub fn new() -> GameOverState {
        return GameOverState {
            render_game_over_sys: RenderGameOverSystem::new(),
        };
    }
}

impl State for GameOverState {
    fn update(&mut self, world: &mut World) -> Transition {
        if let Some(key) = world.key {
            match key {
                Keycode::Escape => return Transition::Switch(Box::new(MainMenuState::new())),
                _ => (),
            }
        }

        return Transition::Continue;
    }

    fn draw(&mut self, world: &mut World) -> Result<String, String> {
        self.render_game_over_sys.execute(world);

        return Ok(String::from("OK"));
    }
}
